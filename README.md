# ATS Notification
This is a RabbitMQ queue worker, that's handle sending notifications to ATS users and candidates. 

## Tests

Once PHP7 development bash is started, the following testing tools are supported by example service:

- Unit tests

  ```bash
  composer tests-unit
  ```
