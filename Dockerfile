FROM dockerregistry.stepstone.com/sp/php7-vanilla:1.3.2.e49d6f2.build_183

#
# Test and production environment Dockerfile
#
# To start service within container run from within service directory
#
# webContainer up prod
#

ADD /service.tar.gz /var/www/html/
RUN chown -R www-data:www-data /var/www/html

# Start liveness socket and worker on container start
CMD /opt/startWorkerAndLiveness.sh src/worker.php