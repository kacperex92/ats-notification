<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-28
 * Time: 02:04 PM
 */

namespace StepStone\Recruiting\ATS\Event;

use Esky\Enum\Enum;

class EventType extends Enum
{
    const CHANGE_APPLICATION_STATUS = 0;
    const CHANGE_HIRING_STATUS = 1;
    const RECRUITER_REMINDER = 2;
}
