<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-28
 * Time: 02:21 PM
 */

namespace StepStone\Recruiting\ATS\Event;

class EventDetails
{
    protected $recruiterId;

    private $rawDetails;


    public function __construct(\stdClass $rawDetails)
    {
        if (!isset($rawDetails->recruiterId)){
            throw new BadEventException('Missing recruiter id!');
        }

        $this->rawDetails = $rawDetails;
        $this->recruiterId = $rawDetails->recruiterId;
    }

    /**
     * @return mixed
     */
    public function getRecruiterId()
    {
        return $this->recruiterId;
    }

    /**
     * @param mixed $recruiterId
     */
    public function setRecruiterId($recruiterId)
    {
        $this->recruiterId = $recruiterId;
    }

    /**
     * @return \stdClass
     */
    public function getRawDetails(): \stdClass
    {
        return $this->rawDetails;
    }
}
