<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-28
 * Time: 02:07 PM
 */

namespace StepStone\Recruiting\ATS\Event;

class Event
{

    /**
     * @var EventType
     */
    private $eventType;

    /**
     * @var EventDetails
     */
    private $eventDetails;

    /**
     * Event constructor.
     * @param EventType $eventType
     * @param EventDetails $eventDetails
     */
    public function __construct(EventType $eventType, EventDetails $eventDetails)
    {
        $this->eventType = $eventType;
        $this->eventDetails = $eventDetails;
    }

    /**
     * @return EventType
     */
    public function getType(): EventType
    {
        return $this->eventType;
    }

    /**
     * @return EventDetails
     */
    public function getDetails()
    {
        return $this->eventDetails;
    }


}
