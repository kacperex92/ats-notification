<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-28
 * Time: 02:38 PM
 */

namespace StepStone\Recruiting\ATS\Event;

use StepStone\Common\StepStoneRuntimeException;

class BadEventException extends StepStoneRuntimeException
{

}
