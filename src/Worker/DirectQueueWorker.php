<?php

namespace StepStone\Recruiting\ATS\Worker;

use StepStone\Common\Diagnostics\LoggerInterface;
use StepStone\Common\ServiceConfigurationInterface;
use StepStone\Queue\BasicQueueWorker;
use StepStone\Queue\Error\QueueMessageErrorHandlerInterface;
use StepStone\Queue\Process\Fork;
use StepStone\Queue\QueueConnectionFactory;
use StepStone\Queue\QueueMessage;
use StepStone\Queue\Server\HttpServer;
use StepStone\Recruiting\ATS\Notification\Notification\NotificationMessageProcessor;

class DirectQueueWorker extends BasicQueueWorker
{

    /**
     * @var NotificationMessageProcessor
     */
    private $messageProcessor;

    public function __construct(
        QueueConnectionFactory $connectionFactory,
        LoggerInterface $logger,
        ServiceConfigurationInterface $config,
        HttpServer $httpServer,
        Fork $processFork,
        QueueMessageErrorHandlerInterface $errorHandler,
        NotificationMessageProcessor $messageProcessor
    ) {
        parent::__construct($connectionFactory, $logger, $config, $httpServer, $processFork, $errorHandler);

        $this->messageProcessor = $messageProcessor;
    }

    /**
     * Function called when message is received
     *
     * @param QueueMessage $queueMessage
     */
    protected function onMessageReceived(QueueMessage $queueMessage): void
    {
        $this->messageProcessor->processMessage($queueMessage);
    }
}
