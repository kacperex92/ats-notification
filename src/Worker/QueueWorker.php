<?php

namespace StepStone\Recruiting\ATS\Worker;

use StepStone\SkeletonWorker\StepStoneQueueWorker;

/**
 * Class QueueWorker
 * @package StepStone\QueueExample
 */
class QueueWorker extends StepStoneQueueWorker
{
    protected function getDependencyInjectionDefinitions(): array
    {
        $main = parent::getDependencyInjectionDefinitions();

        return array_merge(
            $main,
            DependencyProvider::getDependencyInjectionDefinitions()
        );
    }
}
