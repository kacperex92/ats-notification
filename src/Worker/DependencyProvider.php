<?php

namespace StepStone\Recruiting\ATS\Worker;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Queue\QueueConnectionFactoryInterface;
use StepStone\Common\Web\RestClientFactoryInterface;
use StepStone\Database\DatabaseConnectionFactory;

use StepStone\Queue\QueueConnectionFactory;
use StepStone\Recruiting\ATS\Application\ApplicationCombineService;
use StepStone\Recruiting\ATS\Application\ApplicationRepository;
use StepStone\Recruiting\ATS\Application\Apply\ApplyRepository;
use StepStone\Recruiting\ATS\Application\Apply\ServiceApplyRepository;
use StepStone\Recruiting\ATS\Application\Evaluation\DatabaseEvaluationRepository;
use StepStone\Recruiting\ATS\Application\Evaluation\EvaluationRepository;
use StepStone\Recruiting\ATS\Application\ExternalSourcesDataProvider;
use StepStone\Recruiting\ATS\Application\Sorting\ApplicationSortByDate;
use StepStone\Recruiting\ATS\Application\Sorting\ApplicationSorter;
use StepStone\Recruiting\ATS\Application\Sorting\EvaluationSortByDate;
use StepStone\Recruiting\ATS\Application\Sorting\EvaluationSorter;
use StepStone\Recruiting\ATS\Listing\DatabaseListingRepository;
use StepStone\Recruiting\ATS\Listing\ListingRepository;
use StepStone\Recruiting\ATS\Notification\CloseHiring\CloseHiringNotificationService;
use StepStone\Recruiting\ATS\Notification\DatabaseNotificationRepository;
use StepStone\Recruiting\ATS\Notification\NotificationRepository;
use StepStone\Recruiting\ATS\Notification\NotificationService;
use StepStone\Recruiting\ATS\Notification\NotificationServiceInterface;
use StepStone\Recruiting\ATS\Recruiter\RecruiterService;
use StepStone\Recruiting\ATS\Recruiter\RecruiterServiceRepository;
use StepStone\Recruiting\ATS\Recruiter\Reminder\DatabaseReminderRepository;
use StepStone\Recruiting\ATS\Recruiter\Reminder\ReminderRepository;
use StepStone\RestClient\GuzzleRestClientFactory;

/**
 * @codeCoverageIgnore
 */
class DependencyProvider
{

    public static function getDependencyInjectionDefinitions(): array
    {
        return array(
            DatabaseConnectionFactoryInterface::class => \DI\object(DatabaseConnectionFactory::class),
            NotificationRepository::class => \DI\object(
                DatabaseNotificationRepository::class
            ),
            NotificationServiceInterface::class => \DI\object(
                NotificationService::class
            ),
            CloseHiringNotificationService::class => \DI\object(CloseHiringNotificationService::class),

            RestClientFactoryInterface::class => \DI\object(
                GuzzleRestClientFactory::class
            ),
            ApplicationRepository::class => \DI\object(
                DatabaseApplicationRepository::class
            ),
            ApplyRepository::class => \DI\object(
                ServiceApplyRepository::class
            ),
            StatusRepository::class => \DI\object(
                DatabaseStatusRepository::class
            ),
            ListingRepository::class => \DI\object(
                DatabaseListingRepository::class
            ),
            ApplicationSorter::class => \DI\object(
                ApplicationSortByDate::class
            ),
            EvaluationSorter::class => \DI\object(
                EvaluationSortByDate::class
            ),
            EvaluationRepository::class => \DI\object(
                DatabaseEvaluationRepository::class
            ),
            RatingRepository::class => \DI\object(
                DatabaseRatingRepository::class
            ),
            ReminderRepository::class => \DI\object(
                DatabaseReminderRepository::class
            ),
            ContactRepository::class => \DI\object(
                DatabaseContactRepository::class
            ),
            ReminderRepository::class => \DI\object(
                DatabaseReminderRepository::class
            ),
            QueueConnectionFactoryInterface::class => \DI\object(
                QueueConnectionFactory::class
            ),
            HtmlSanitizer::class => \DI\object(
                HtmlPurifierHtmlSanitizer::class
            )->constructor(
                new HTMLPurifier(),
                '/tmp'
            ),
            WhiteListProviderInterface::class => \DI\object(
                WhiteListProvider::class
            ),
            RecruiterService::class => \DI\object(
                RecruiterServiceRepository::class
            ),
            JobMessengerRepository::class => \DI\object(
                JobMessengerServiceRepository::class
            ),
            ExternalSourcesDataProvider::class => \DI\object(
                ExternalSourcesDataProvider::class
            ),
            ApplicationCombineService::class => \DI\object(
                ApplicationCombineService::class
            ),
            FeaturesConfiguration::class => \DI\object(
                YamlConfiguration::class
            ),
        );
    }
}
