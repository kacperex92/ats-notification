<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\CloseHiring;

use StepStone\Recruiting\ATS\Application\Application;
use StepStone\Recruiting\ATS\Application\ApplicationService;
use StepStone\Recruiting\ATS\Application\Status\ApplicationCollectionStatus;
use StepStone\Recruiting\ATS\Application\Status\ApplicationStatus;
use StepStone\Recruiting\ATS\Event\Event;
use StepStone\Recruiting\ATS\Listing\ListingRepository;
use StepStone\Recruiting\ATS\Notification\NotificationData;
use StepStone\Recruiting\ATS\Notification\NotificationModule;
use StepStone\Recruiting\ATS\Notification\NotificationRepository;
use StepStone\Recruiting\ATS\Notification\NotificationServiceInterface;
use StepStone\Recruiting\ATS\Notification\YamlNotificationServiceConfiguration;
use StepStone\Recruiting\ATS\Recommender\ServiceRecommenderRepository;
use StepStone\Recruiting\ATS\Recruiter\RecruiterService;

class CloseHiringNotificationService implements NotificationModule
{
    private const LIST_STATUSES_TO_SEND_NOTIFICATION = [ApplicationStatus::KEPT, ApplicationStatus::NEW];
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;
    /**
     * @var ApplicationService
     */
    private $applicationService;
    /**
     * @var ListingRepository
     */
    private $listingRepository;
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;
    /**
     * @var ServiceRecommenderRepository
     */
    private $recommenderService;
    /**
     * @var RecruiterService
     */
    private $recruiterService;
    /**
     * @var YamlNotificationServiceConfiguration
     */
    private $config;

    public function __construct(
        NotificationServiceInterface $notificationService,
        ApplicationService $applicationService,
        ListingRepository $listingRepository,
        YamlNotificationServiceConfiguration $config,
        NotificationRepository $notificationRepository,
        ServiceRecommenderRepository $recommenderService,
        RecruiterService $recruiterService
    ) {
        $this->notificationService = $notificationService;
        $this->applicationService = $applicationService;
        $this->listingRepository = $listingRepository;
        $this->notificationRepository = $notificationRepository;
        $this->recommenderService = $recommenderService;
        $this->recruiterService = $recruiterService;
        $this->config = $config;
    }

    /**
     * @param string $listingId
     * @param string $recruiterId
     * @param \StepStone\Recruiting\ATS\Notification\NotificationData $notificationData
     *
     * @return bool
     * @throws \Exception
     */
    public function sendCloseHiringEmails(
        string $listingId,
        string $recruiterId,
        NotificationData $notificationData
    ) : bool {

        // Get applications
        $applications = $this->applicationService->getApplications(
            $listingId,
            $this->getApplicationStatusesToSendNotification()
        );

        $sendNow = $this->shouldSendNow($notificationData);

        foreach ($applications->applications as $application) {
            if ($sendNow) {
                $message = $this->getQueueMessage(
                    $application,
                    $listingId,
                    $recruiterId,
                    $notificationData
                );

                $this->sendToQueue($message);
            } else {
                $this->addToDelayQueue($application->id, $recruiterId, $notificationData);
            }
        }

        return true;
    }

    private function shouldSendNow(NotificationData $notificationData) : bool
    {
        return null === $notificationData->getSendDelayInDays() || 0 === $notificationData->getSendDelayInDays();
    }

    private function getQueueMessage(
        Application $application,
        string $listingId,
        string $recruiterId,
        NotificationData $notificationData
    ): CloseHiringNotificationMessage {

        $recruiterDetails = $this->recruiterService->getRecruiterDetails($recruiterId);

        $listingDetails = $this->listingRepository->getListingDetails($listingId);

        $notificationMessage = new CloseHiringNotificationMessage();
        $notificationMessage->setApplicationId($this->applicationService->getApplication($application->id)->id);
        $notificationMessage->setEmail($application->candidateEmail);
        $notificationMessage->setCampaign($this->config->getDiscardedApplicationCampaignName());
        $notificationMessage->setLang($this->config->getEmailLanguage());
        $notificationMessage->setListingTitle($listingDetails->getTitle());
        $notificationMessage->setListingId($listingDetails->getId());
        $notificationMessage->setRecruiterFirstName($recruiterDetails->getFirstName());
        $notificationMessage->setRecruiterLastName($recruiterDetails->getLastName());
        $notificationMessage->setRecruiterId($recruiterDetails->getId());
        $notificationMessage->setEmailContent($notificationData->getEmailContent());
        $notificationMessage->setRecommendedListings($this->recommenderService->getRecommendedListings($listingId));

        return $notificationMessage;
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function getApplicationStatusesToSendNotification() : array
    {
        $statuses = [];

        foreach (static::LIST_STATUSES_TO_SEND_NOTIFICATION as $statusName) {
            $statuses[] = new ApplicationCollectionStatus($statusName);
        }
        return $statuses;
    }

    private function sendToQueue(CloseHiringNotificationMessage $message) : void
    {
        $messageId = $this->notificationService->getMessageId();
        $this->notificationService->send($messageId, $message);
    }

    private function addToDelayQueue(
        string $applicationId,
        string $recruiterId,
        NotificationData $notificationData
    ) : bool {

        $this->notificationRepository->insertNotification(
            $applicationId,
            $recruiterId,
            $notificationData->getEmailContent(),
            $notificationData->getSendDate()
        );

        return true;
    }

    public function sendNotification(Event $event): void
    {
        // TODO: Implement sendNotification() method.
    }
}
