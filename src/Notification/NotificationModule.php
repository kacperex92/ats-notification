<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-29
 * Time: 01:31 PM
 */

namespace StepStone\Recruiting\ATS\Notification;

use StepStone\Recruiting\ATS\Event\Event;

interface NotificationModule
{

    public function sendNotification(Event $event): void;
}
