<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

use StepStone\Common\FileSystem\FileSystemFactoryInterface;
use Assert\Assert;

class YamlNotificationServiceConfiguration implements NotificationServiceConfiguration
{
    private const LISTING_ID_NEEDLE = '{listingId}';

    private const USE_DEVELOPMENT_EMAIL_KEY = 'useDevelopmentEmail';
    private const SEND_TO_RECRUITER_KEY = 'sendEmailToRecruiter';
    private const RECRUITER_REMINDER_CAMPAIGN_NAME_KEY = 'recruiterReminderCampaignName';
    private const DISCARDED_APPLICATION_CAMPAIGN_NAME_KEY = 'discardedApplicationCampaignName';
    private const DOMAIN_KEY = 'domain';
    private const EMAIL_LANGUAGE_KEY = 'emailLanguage';
    private const NUMBER_OF_REQUESTED_RECOMMENDED_LISTINGS = 'numberOfRequestedRecommendedListings';
    private const ATS_BASE_URL = 'atsBaseUrl';

    private const FILE_SYSTEM_NAME = 'businessConfiguration';
    private const NOTIFICATION_REPOSITORY_CONFIG_FILE_NAME = 'notification.yaml';

    /**
     * @var bool
     */
    private $useDevelopmentEmail;
    private $sendEmailToRecruiter;

    /**
     * @var string
     */
    private $recruiterReminderCampaignName;
    private $discardedApplicationCampaignName;
    private $domain;
    private $emailLanguage;
    private $numberOfRequestedRecommendedListings;
    private $atsBaseUrl;

    public function __construct(FileSystemFactoryInterface $connectionFactory)
    {
        $cacheConnection = $connectionFactory->getFileCacheService(static::FILE_SYSTEM_NAME);
        $this->parseConfig($cacheConnection->parseCacheableYaml(static::NOTIFICATION_REPOSITORY_CONFIG_FILE_NAME));
    }

    private function parseConfig(array $rawConfig) : void
    {
        $this->useDevelopmentEmail = $this->getBooleanValue(static::USE_DEVELOPMENT_EMAIL_KEY, $rawConfig);
        $this->sendEmailToRecruiter = $this->getBooleanValue(static::SEND_TO_RECRUITER_KEY, $rawConfig);

        $this->recruiterReminderCampaignName = $this->getStringValue(
            static::RECRUITER_REMINDER_CAMPAIGN_NAME_KEY,
            $rawConfig
        );
        $this->discardedApplicationCampaignName = $this->getStringValue(
            static::DISCARDED_APPLICATION_CAMPAIGN_NAME_KEY,
            $rawConfig
        );
        $this->domain = $this->getStringValue(static::DOMAIN_KEY, $rawConfig);
        $this->emailLanguage = $this->getStringValue(static::EMAIL_LANGUAGE_KEY, $rawConfig);
        $this->numberOfRequestedRecommendedListings = $this->getIntValue(
            static::NUMBER_OF_REQUESTED_RECOMMENDED_LISTINGS,
            $rawConfig
        );
        $this->atsBaseUrl = $this->getStringValue(static::ATS_BASE_URL, $rawConfig);
    }

    private function getBooleanValue(string $keyName, array $rawConfig) : bool
    {
        Assert::that($rawConfig)
            ->keyExists(
                $keyName,
                'Can not find ' . $keyName . ' key in ' . static::NOTIFICATION_REPOSITORY_CONFIG_FILE_NAME
            );

        $value = $rawConfig[$keyName];

        Assert::that($value)
            ->boolean();
        return $value;
    }

    private function getStringValue(string $keyName, array $rawConfig) : string
    {
        Assert::that($rawConfig)
            ->keyExists(
                $keyName,
                'Can not find ' . $keyName . ' key in ' . static::NOTIFICATION_REPOSITORY_CONFIG_FILE_NAME
            );

        $value = $rawConfig[$keyName];

        Assert::that($value)
            ->notEmpty()
            ->string();
        return $value;
    }

    private function getIntValue(string $keyName, array $rawConfig) : int
    {
        Assert::that($rawConfig)
            ->keyExists(
                $keyName,
                'Can not find ' . $keyName . ' key in ' . static::NOTIFICATION_REPOSITORY_CONFIG_FILE_NAME
            );

        $value = $rawConfig[$keyName];

        Assert::that($value)
            ->notEmpty()
            ->integer();
        return $value;
    }

    public function useDevelopmentEmail(): bool
    {
        return $this->useDevelopmentEmail;
    }

    public function getRecruiterReminderCampaignName() : string
    {
        return $this->recruiterReminderCampaignName;
    }

    public function getDiscardedApplicationCampaignName() : string
    {
        return $this->discardedApplicationCampaignName;
    }

    public function sendEmailToRecruiterEnabled(): bool
    {
        return $this->sendEmailToRecruiter;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getEmailLanguage(): string
    {
        return $this->emailLanguage;
    }

    public function getNumberOfRequestedRecommendedListings() : int
    {
        return $this->numberOfRequestedRecommendedListings;
    }

    public function getAtsBaseUrl(string $listingId): string
    {
        return str_replace(
            static::LISTING_ID_NEEDLE,
            $listingId,
            $this->atsBaseUrl
        );
    }
}
