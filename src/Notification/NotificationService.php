<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

use Psr\Container\ContainerInterface;
use StepStone\Common\RequestContextInterface;
use StepStone\Recruiting\ATS\Event\BadEventException;
use StepStone\Recruiting\ATS\Event\Event;
use StepStone\Recruiting\ATS\Event\EventType;
use StepStone\Recruiting\ATS\Notification\CloseHiring\CloseHiringNotificationMessage;
use StepStone\Recruiting\ATS\Notification\CloseHiring\CloseHiringNotificationService;
use StepStone\Recruiting\ATS\Notification\DiscardApplication\DiscardApplicationNotificationService;
use StepStone\Recruiting\ATS\Notification\Parser\CandidateEmailContentParser;
use StepStone\Recruiting\ATS\Notification\RecruiterReminder\RecruiterReminderNotificationService;

class NotificationService implements NotificationServiceInterface
{
    private const AT_REPLACEMENT_CHAR = '-';
    private const DISPOSABLE_EMAIL_DOMAIN = 'in.fistep.com';

    /**
     * @var NotificationTriggerPublisher
     */
    private $notificationTriggerPublisher;

    /**
     * @var YamlNotificationServiceConfiguration
     */
    protected $config;
    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @var RequestContextInterface
     */
    private $requestContext;

    /**
     * @var CandidateEmailContentParser
     */
    private $candidateEmailContentParser;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        YamlNotificationServiceConfiguration $config,
        NotificationTriggerPublisher $notificationTriggerPublisher,
        RequestContextInterface $requestContext,
        NotificationRepository $notificationRepository,
        CandidateEmailContentParser $candidateEmailContentParser,
        ContainerInterface $container
    ) {
        $this->notificationTriggerPublisher = $notificationTriggerPublisher;
        $this->config = $config;
        $this->requestContext = $requestContext;
        $this->notificationRepository = $notificationRepository;
        $this->candidateEmailContentParser = $candidateEmailContentParser;
        $this->container = $container;
    }

     public function processEvent(Event $event) : void
    {
        $module = $this->getModuleForEvent($event->getType());

        $module->sendNotification($event);
    }

    private function getModuleForEvent(EventType $eventType)
    {
        switch ($eventType->getValue()) {
            case EventType::CHANGE_APPLICATION_STATUS :
                return $this->container->get(DiscardApplicationNotificationService::class);
            case EventType::CHANGE_HIRING_STATUS :
                return $this->container->get(CloseHiringNotificationService::class);
            case EventType::RECRUITER_REMINDER :
                return $this->container->get(RecruiterReminderNotificationService::class);
            default :
                throw new BadEventException('Not supported Event Type: ' . $eventType->getName());
        }
    }

    public function send(string $messageId, NotificationMessage $message) : void
    {
        if ($this->config->useDevelopmentEmail()) {
            $email = $this->convertToDevMail($message->getEmail());
            $message->setEmail($email);
        }

        if ($message instanceof CloseHiringNotificationMessage) {
            $this->fillTags($message);
        }

        $queueMessage = $message->toNotificationMessage();

        $this->notificationTriggerPublisher->publishMessage($messageId, $queueMessage);
    }

    public function getMessageId() : string
    {
        return $this->requestContext->getRequestId()->toString();
    }

    public function deleteNotification(string $applicationId) : bool
    {
        return $this->notificationRepository->deleteNotification($applicationId);
    }

    public function getMessage(string $applicationId) : array
    {
        return $this->notificationRepository->getNotification($applicationId) ?? [];
    }

    public function addNotification(
        string $applicationId,
        string $recruiterId,
        NotificationData $notificationData
    ) : bool {
        return $this->notificationRepository->insertNotification(
            $applicationId,
            $recruiterId,
            $notificationData->getEmailContent(),
            $notificationData->getSendDate()
        );
    }

    public function getMessagesToSend() : ?array
    {
        return $this->notificationRepository->getMessagesToSend();
    }

    private function convertToDevMail(string $email) : string
    {
        return $this->isDevEmailAlready($email)
            ? $email
            : str_replace('@', static::AT_REPLACEMENT_CHAR, $email) . '@' . static::DISPOSABLE_EMAIL_DOMAIN;
    }

    private function isDevEmailAlready(string $email) : bool
    {
        // checks if email already ends with our disposable domain
        $needle = '@' . static::DISPOSABLE_EMAIL_DOMAIN;
        return (substr($email, -strlen($needle)) === $needle);
    }

    private function fillTags(CloseHiringNotificationMessage $message) : CloseHiringNotificationMessage
    {
        $parsedEmailContent = $this->candidateEmailContentParser->parseContent(
            $message->getEmailContent(),
            $message->getApplicationId()
        );

        $message->setEmailContent($parsedEmailContent);

        return $message;
    }
}
