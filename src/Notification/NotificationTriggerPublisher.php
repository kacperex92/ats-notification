<?php

namespace StepStone\Recruiting\ATS\Notification;

use PhpAmqpLib\Message\AMQPMessage;
use StepStone\Common\Diagnostics\LoggerInterface;
use StepStone\Common\Queue\QueueConnectionConfigurationDto;
use StepStone\Common\Queue\QueueConnectionFactoryInterface;
use StepStone\Common\ServiceConfigurationInterface;
use StepStone\Queue\QueueMessage;
use StepStone\Queue\Traits\GetQueueExchangeName;
use StepStone\Queue\Traits\PickQueueName;

class NotificationTriggerPublisher
{
    use PickQueueName;
    use GetQueueExchangeName;

    private const CONNECTION_NAME = 'notification_trigger';

    /**
     * @var QueueConnectionFactoryInterface
     */
    private $connectionFactory;

    /**
     * @var ServiceConfigurationInterface
     */
    private $config;

    /**
     * @var QueueConnectionConfigurationDto
     */
    protected $configurationDto;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        QueueConnectionFactoryInterface $connectionFactory,
        ServiceConfigurationInterface $config,
        LoggerInterface $logger
    ) {
        $this->connectionFactory = $connectionFactory;
        $this->config = $config;
        $this->logger = $logger;
    }


    public function publishMessage(string $messageId, array $message) : void
    {
        $this->configurationDto = $this->config->getQueueConnection(static::CONNECTION_NAME);
        $connection = $this->connectionFactory->getConnection(static::CONNECTION_NAME);
        $queueName = $this->pickQueueName();

        try {
            $exchangeName = $this->getQueueExchangeName($queueName);
            $channel = $connection->channel();

            $queueMessage = new QueueMessage($message, $messageId);
            $amqpMsg = new AMQPMessage($queueMessage->toJson(), $this->getMessageOptions());
            $channel->basic_publish($amqpMsg, $exchangeName);

            $this->logger->info('Notification message published', $message);
        } finally {
            $channel->close();
            $connection->close();
        }
    }

    private function getMessageOptions(): array
    {
        return [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ];
    }
}
