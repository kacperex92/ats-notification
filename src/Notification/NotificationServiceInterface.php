<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

/**
 * Interface EmailNotificationClientInterface
 * @package StepStone\JobApplicationEmailWorker\EmailNotificationClient
 * @codeCoverageIgnore
 */
interface NotificationServiceInterface
{
    public function send(string $messageId, NotificationMessage $message) : void;

    public function getMessageId() : string;


    public function deleteNotification(string $applicationId) : bool;

    public function addNotification(
        string $applicationId,
        string $recruiterId,
        NotificationData $notificationData
    ) : bool;

    public function getMessagesToSend(): ?array;
}
