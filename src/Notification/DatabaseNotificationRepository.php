<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Data\DatabaseConnectionInterface;

class DatabaseNotificationRepository implements NotificationRepository
{

    /**
     * @var DatabaseConnectionInterface
     */
    private $connection;

    public function __construct(
        DatabaseConnectionFactoryInterface $connectionFactory
    ) {
        $this->connection = $connectionFactory->getConnection('stepstoneAts');
    }

    public function deleteNotification(string $applicationId): bool
    {
        $notificationRequest = $this->connection->prepare(
            'DELETE FROM
                          %schema%.NotificationEmail
                        WHERE
                            ApplicationId = :applicationId'
        );
        $notificationRequest->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        return $notificationRequest->execute();
    }

    /**
     * @param string $applicationId
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function setAsSent(string $applicationId): bool
    {
        $databaseResult = $this->connection->prepare(
            'UPDATE
                %schema%.Application
            SET
                [DiscardedMailSent] = 1
            WHERE
                [ApplicationId] = :applicationId'
        );
        $databaseResult->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        return $databaseResult->execute();
    }


    public function getNotification(string $applicationId): ?array
    {
        $notificationRequest = $this->connection->prepare(
            'SELECT TOP 1
                         ApplicationId,
                         RecruiterId,
                         EmailContent,
                         SendDate
                        FROM
                          %schema%.NotificationEmail
                        WHERE
                            ApplicationId = :applicationId'
        );

        $notificationRequest->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        $notificationRequest->execute();
        $resultRow = $notificationRequest->fetch();
        return $resultRow ?: [];
    }

    public function getSentStatus(string $applicationId): bool
    {
        $databaseResult = $this->connection->prepare(
            'SELECT
                    [DiscardedMailSent] as [result]
                FROM
                    %schema%.Application
                WHERE
                    ApplicationId = :applicationId'
        );

        $databaseResult->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        $databaseResult->execute();

        return (bool)$databaseResult->fetch()['result'];
    }

    public function insertNotification(
        string $applicationId,
        string $recruiterId,
        string $emailContent,
        \DateTime $sendDate
    ) : bool {
        $notificationRequest = $this->connection->prepare(
            'INSERT INTO %schema%.NotificationEmail
                       ( ApplicationId,
                         RecruiterId,
                         EmailContent,
                         SendDate
                        )
                        VALUES
                        (
                            :applicationId,
                            :recruiterId,
                            :emailContent,
                            :sendDate
                        )'
        );

        $notificationRequest->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        $notificationRequest->bindValue(
            ':recruiterId',
            $recruiterId,
            \PDO::PARAM_INT
        );

        $notificationRequest->bindValue(
            ':emailContent',
            $emailContent
        );

        $notificationRequest->bindValue(
            ':sendDate',
            date('Y-m-d H:i:s', $sendDate->getTimestamp())
        );

        return $notificationRequest->execute();
    }

    /**
     * @return array|null
     * @throws \InvalidArgumentException
     */
    public function getMessagesToSend(): ?array
    {
        $notificationRequest = $this->connection->prepare(
            'SELECT
                noti.ApplicationId,
                noti.RecruiterId,
                noti.EmailContent,
                app.ListingId as ListingId
            FROM %schema%.NotificationEmail noti
            INNER JOIN %schema%.Application app ON noti.ApplicationId = app.ApplicationID
            WHERE
                CONVERT(date, noti.SendDate) <= CONVERT(date, GETDATE())'
        );

        $notificationRequest->execute();

        return $notificationRequest->fetchAll();
    }
}
