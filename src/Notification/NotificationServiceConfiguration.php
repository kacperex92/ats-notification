<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

interface NotificationServiceConfiguration
{
    /**
     * Determine if messages should be sent to safe development email
     * instead of original recipient
     * @return bool
     */
    public function useDevelopmentEmail() : bool;

    /**
     * Get mailing campaign name for recruiter reminder emails
     * @return string name of the campaign
     */
    public function getRecruiterReminderCampaignName() : string;

    /**
     * check if worker should send message to recruiter
     * @return bool
     */
    public function sendEmailToRecruiterEnabled(): bool;

    /**
     * get domain name for call to action links
     * @return string
     */
    public function getDomain(): string;

    /**
     * get language in which email should be sent
     * @return string
     */
    public function getEmailLanguage() : string;

    /**
     * get base for url to mini ats service
     * @param string $listingId
     * @return string
     */
    public function getAtsBaseUrl(string $listingId): string;
}
