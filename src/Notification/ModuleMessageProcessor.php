<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-28
 * Time: 02:53 PM
 */

namespace StepStone\Recruiting\ATS\Notification\Notification;

use StepStone\Recruiting\ATS\Event\Event;

interface ModuleMessageProcessor
{
    public function processEvent(Event $event) : void;
}
