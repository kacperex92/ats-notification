<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

/**
 * Class NotificationData
 * @package StepStone\Recruiting\ATS\Notification
 * @codeCoverageIgnore
 */
class NotificationData
{

    /**
     * @var string
     */
    private $emailContent;


    /**
     * @var null|\DateTime
     */
    private $sendDate;

    /**
     * @var null|int
     */
    private $sendDelayInDays;

    /**
     * NotificationData constructor.
     * @param string $emailContent
     * @param null|int $sendDelayInDays
     * @throws \Exception
     */
    public function __construct(string $emailContent, ?int $sendDelayInDays = null)
    {
        $this->emailContent = $emailContent;
        $this->sendDate = new \DateTime();
        $this->sendDelayInDays = $sendDelayInDays;

        if (isset($sendDelayInDays) && $sendDelayInDays > 0) {
            $this->sendDate->add(new \DateInterval('P' . $sendDelayInDays . 'D'));
        }
    }


    /**
     * @return string
     */
    public function getEmailContent(): string
    {
        return $this->emailContent;
    }

    /**
     * @return null|\DateTime
     */
    public function getSendDate(): ?\DateTime
    {
        return $this->sendDate;
    }

    /**
     * @return null|int
     */
    public function getSendDelayInDays(): ?int
    {
        return $this->sendDelayInDays;
    }

    /**
     * @param string $emailContent
     */
    public function setEmailContent(string $emailContent): void
    {
        $this->emailContent = $emailContent;
    }
}
