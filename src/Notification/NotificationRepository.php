<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

interface NotificationRepository
{
    public function setAsSent(string $applicationId) : bool;

    public function getSentStatus(string $applicationId) : bool;

    public function deleteNotification(string $applicationId) : bool;

    public function getNotification(string $applicationId): ?array;

    public function insertNotification(
        string $applicationId,
        string $recruiterId,
        string $emailContent,
        \DateTime $sendDate
    ): bool;

    public function getMessagesToSend(): ?array;
}
