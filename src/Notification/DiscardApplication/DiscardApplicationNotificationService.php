<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\DiscardApplication;

use StepStone\Common\Diagnostics\LoggerInterface;
use StepStone\Common\Web\RestCommunicationException;
use StepStone\Recruiting\ATS\Application\ApplicationService;
use StepStone\Recruiting\ATS\Event\Event;
use StepStone\Recruiting\ATS\Notification\NotificationModule;
use StepStone\Recruiting\ATS\Recommender\RecommendedListings;
use StepStone\Recruiting\ATS\Recommender\ServiceRecommenderRepository;
use StepStone\Recruiting\ATS\Listing\ListingService;
use StepStone\Recruiting\ATS\Notification\NotificationRepository;
use StepStone\Recruiting\ATS\Notification\NotificationServiceInterface;
use StepStone\Recruiting\ATS\Notification\YamlNotificationServiceConfiguration;
use StepStone\Recruiting\ATS\Recruiter\RecruiterDetails;
use StepStone\Recruiting\ATS\Recruiter\RecruiterService;

class DiscardApplicationNotificationService implements NotificationModule
{
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;

    /**
     * @var ApplicationService
     */
    private $applicationService;

    /**
     * @var ListingService
     */
    private $listingService;

    /**
     * @var YamlNotificationServiceConfiguration
     */
    private $config;

     /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * @var ServiceRecommenderRepository
     */
    private $recommenderService;

    /**
     * @var RecruiterService
     */
    private $recruiterService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RecruiterReminderNotificationService constructor.
     * @param NotificationServiceInterface $notificationService
     * @param ApplicationService $applicationService
     * @param ListingService $listingService
     * @param YamlNotificationServiceConfiguration $config
     * @param NotificationRepository $notificationRepository
     * @param ServiceRecommenderRepository $recommenderService
     * @param RecruiterService $recruiterService
     * @param LoggerInterface $logger
     */
    public function __construct(
        NotificationServiceInterface $notificationService,
        ApplicationService $applicationService,
        ListingService $listingService,
        YamlNotificationServiceConfiguration $config,
        NotificationRepository $notificationRepository,
        ServiceRecommenderRepository $recommenderService,
        RecruiterService $recruiterService,
        LoggerInterface $logger
    ) {
        $this->notificationService = $notificationService;
        $this->applicationService = $applicationService;
        $this->listingService = $listingService;
        $this->config = $config;
        $this->notificationRepository = $notificationRepository;
        $this->recommenderService = $recommenderService;
        $this->recruiterService = $recruiterService;
        $this->logger = $logger;
    }

    /**
     * @param string $applicationId
     * @param string $listingId
     * @param string|null $emailContent
     * @param string $recruiterId
     */
    private function sendDiscardedEmail(
        string $applicationId,
        string $listingId,
        string $recruiterId,
        string $emailContent = null
    ) : void {
        if ($this->shouldSendEmail($applicationId)) {
            $application = $this->applicationService->getApplication($applicationId);
            $listing = $this->listingService->getListingDetails($listingId);

            $recommendedListings = $this->getRecommendedListings($listingId);

            $recruiterDetails = $this->recruiterService->getRecruiterDetails($recruiterId);

            $message = $this->getQueueMessage(
                $application->candidateEmail,
                $listing->getId(),
                $application->id,
                $recruiterDetails,
                $recommendedListings,
                $emailContent
            );

            $this->notificationService->send(
                $this->notificationService->getMessageId(),
                $message
            );
            $this->notificationRepository->setAsSent($applicationId);
        }
    }

    public function shouldSendEmail(string $applicationId) : bool
    {
//        return !$this->notificationRepository->getSentStatus($applicationId);
        return true;
    }

    private function getRecommendedListings(string $listingId) : RecommendedListings
    {
        try {
            $recommendedListings = $this->recommenderService->getRecommendedListings($listingId);
        } catch (RestCommunicationException $e) {
            $this->logger->error(
                "Cant get recommended listings for listing: $listingId"
            );
            $this->logger->error($e->getMessage(), ['error' => $e]);
        }
        return $recommendedListings ?? new RecommendedListings([]);
    }

    private function getQueueMessage(
        string $email,
        string $listingId,
        string $applicationId,
        RecruiterDetails $recruiterDetails,
        RecommendedListings $recommendedListings,
        ?string $emailContent
    ): DiscardApplicationNotificationMessage {
        $notificationMessage = new DiscardApplicationNotificationMessage();
        $notificationMessage->setEmail($email);
        $notificationMessage->setCampaign($this->config->getDiscardedApplicationCampaignName());
        $notificationMessage->setLang($this->config->getEmailLanguage());
        $notificationMessage->setListingId($listingId);
        $notificationMessage->setApplicationId($applicationId);
        $notificationMessage->setRecruiterId($recruiterDetails->getId());
        $notificationMessage->setRecruiterFirstName($recruiterDetails->getFirstName());
        $notificationMessage->setRecruiterLastName($recruiterDetails->getLastName());
        $notificationMessage->setRecommendedListings($recommendedListings);

        if (null === $emailContent) {
            $emailContent = '';
        }
        $notificationMessage->setEmailContent($emailContent);

        return $notificationMessage;
    }

    public function sendNotification(Event $event): void
    {
        $discardEvent = DiscardEvent::generateFromEvent($event);

        $this->sendDiscardedEmail(
          $discardEvent->getApplicationId(),
          $discardEvent->getListingId(),
          $discardEvent->getRecruiterId(),
          $discardEvent->getNotificationMessage()
        );
    }
}
