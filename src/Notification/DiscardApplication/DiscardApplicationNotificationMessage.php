<?php
/**
 * Copyright Stepstone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\DiscardApplication;

use StepStone\Recruiting\ATS\Notification\NotificationMessage;
use StepStone\Recruiting\ATS\Recommender\RecommendedListings;

class DiscardApplicationNotificationMessage extends NotificationMessage
{

    /**
     * @var string
     */
    private $recruiterId;

    /**
     * @var string
     */
    private $applicationId;

    /**
     * @var string
     */
    private $emailContent;

    /**
     * @var string
     */
    private $recruiterFirstName;

    /**
     * @var string
     */
    private $recruiterLastName;

    /**
     * @var RecommendedListings
     */
    private $recommendedListings;

    public function getRecruiterId() : string
    {
        return $this->recruiterId;
    }

    public function setRecruiterId(string $recruiterId) : void
    {
        $this->recruiterId = $recruiterId;
    }

    public function getApplicationId() : string
    {
        return $this->applicationId;
    }

    public function setApplicationId(string $applicationId) : void
    {
        $this->applicationId = $applicationId;
    }

    public function getRecommendedListings() : RecommendedListings
    {
        return $this->recommendedListings;
    }

    public function setRecommendedListings(RecommendedListings $recommendedListings) : void
    {
        $this->recommendedListings = $recommendedListings;
    }

    /**
     * @return string
     */
    public function getEmailContent() : string
    {
        return $this->emailContent;
    }

    /**
     * @param string $emailContent
     */
    public function setEmailContent(?string $emailContent) : void
    {
        $this->emailContent = $emailContent;
    }

    /**
     * @return string
     */
    public function getRecruiterFirstName(): string
    {
        return $this->recruiterFirstName;
    }

    /**
     * @param string $recruiterFirstName
     */
    public function setRecruiterFirstName(string $recruiterFirstName)
    {
        $this->recruiterFirstName = $recruiterFirstName;
    }

    /**
     * @return string
     */
    public function getRecruiterLastName(): string
    {
        return $this->recruiterLastName;
    }

    /**
     * @param string $recruiterLastName
     */
    public function setRecruiterLastName(string $recruiterLastName)
    {
        $this->recruiterLastName = $recruiterLastName;
    }


    /**
     * @return array
     */
    public function toNotificationMessage() : array
    {
        $result = parent::toNotificationMessage();
        $result['parameters']['EMAIL_DATA_CONTENT'] = nl2br($this->emailContent);
        $result['parameters']['RECRUITER_ID'] = $this->recruiterId;
        $result['parameters']['APPLICATION_ID'] = $this->applicationId;
        $result['parameters']['RECOMMENDED_LISTINGS'] = $this->recommendationsToMessage();
        $result['parameters']['RECRUITER_FIRSTNAME'] = $this->recruiterFirstName;
        $result['parameters']['RECRUITER_LASTNAME'] = $this->recruiterLastName;
        return $result;
    }

    private function recommendationsToMessage() : string
    {
        $result = '[';
        if (isset($this->recommendedListings)) {
            $result .= implode(',', $this->recommendedListings->getListings());
        }
        $result .= ']';

        return $result;
    }
}
