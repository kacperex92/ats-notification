<?php
/**
 * Created by IntelliJ IDEA.
 * User: droska01
 * Date: 2017-11-29
 * Time: 01:41 PM
 */

namespace StepStone\Recruiting\ATS\Notification\DiscardApplication;

use StepStone\Recruiting\ATS\Event\Event;

class DiscardEvent
{

    /**
     * @var string
     */
    private $applicationId;
    private $listingId;
    private $recruiterId;
    private $notificationMessage;

    /**
     * DiscardEvent constructor.
     * @param string $applicationId
     * @param $listingId
     * @param $recruiterId
     * @param $notificationMessage
     */
    public function __construct($applicationId, $listingId, $recruiterId, $notificationMessage)
    {
        $this->applicationId = $applicationId;
        $this->listingId = $listingId;
        $this->recruiterId = $recruiterId;
        $this->notificationMessage = $notificationMessage;
    }


    public static function generateFromEvent(Event $event)
    {
        return new DiscardEvent(
            $event->getDetails()->getRawDetails()->applicationId,
            $event->getDetails()->getRawDetails()->listingId,
            $event->getDetails()->getRawDetails()->recruiterId,
            $event->getDetails()->getRawDetails()->notificationMessage
        );
    }

    /**
     * @return string
     */
    public function getApplicationId(): string
    {
        return $this->applicationId;
    }

    /**
     * @return mixed
     */
    public function getListingId()
    {
        return $this->listingId;
    }

    /**
     * @return mixed
     */
    public function getRecruiterId()
    {
        return $this->recruiterId;
    }

    /**
     * @return mixed
     */
    public function getNotificationMessage()
    {
        return $this->notificationMessage;
    }
}
