<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\Parser;

class TagsReplacer
{
    /**
     * @param string $contendToParse
     * @param array $tagsMap
     *
     * @return string
     */
    public function replaceTags(string $contendToParse, array $tagsMap) : string
    {
        $placeholders = array_keys($tagsMap);
        foreach ($placeholders as &$placeholder) {
            $placeholder = strtoupper("{{$placeholder}}");
        }
        return str_replace($placeholders, array_values($tagsMap), $contendToParse);
    }
}
