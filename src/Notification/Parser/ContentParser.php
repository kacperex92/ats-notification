<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\Parser;

interface ContentParser
{
    public function parseContent(string $contendToParse, string $applicationId) : string;
}
