<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\Parser;

use StepStone\Recruiting\ATS\Application\Apply\ServiceApplyRepository;

class CandidateEmailContentParser implements ContentParser
{
    /**
     * @var TagsReplacer
     */
    private $tagsReplacer;

    /**
     * @var ServiceApplyRepository
     */
    private $serviceApplyRepository;

    /**
     * CandidateEmailContentParser constructor.
     *
     * @param TagsReplacer $tagsReplacer
     * @param ServiceApplyRepository $serviceApplyRepository
     */
    public function __construct(TagsReplacer $tagsReplacer, ServiceApplyRepository $serviceApplyRepository)
    {
        $this->tagsReplacer = $tagsReplacer;
        $this->serviceApplyRepository = $serviceApplyRepository;
    }

    public function parseContent(string $contendToParse, string $applicationId): string
    {
        $tagsMap = $this->getTagsMap($applicationId);

        return $this->tagsReplacer->replaceTags($contendToParse, $tagsMap);
    }

    private function getTagsMap(string $applicationId) : array
    {
        return [
            'candidate name' => $this->serviceApplyRepository->getApplication($applicationId)->candidateFirstName
        ];
    }
}
