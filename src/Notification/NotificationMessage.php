<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification;

class NotificationMessage
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $campaign;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var string
     */
    private $listingTitle;

    /**
     * @var string
     */
    private $listingId;

    /**
     * @return string
     */
    public function getListingId(): string
    {
        return $this->listingId;
    }

    /**
     * @param string $listingId
     */
    public function setListingId(string $listingId): void
    {
        $this->listingId = $listingId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getCampaign(): string
    {
        return $this->campaign;
    }

    public function setCampaign(string $campaign): void
    {
        $this->campaign = $campaign;
    }

    public function getLang(): string
    {
        return $this->lang;
    }

    public function setLang(string $lang): void
    {
        $this->lang = strtoupper($lang);
    }

    public function getListingTitle(): string
    {
        return $this->listingTitle;
    }

    public function setListingTitle(string $listingTitle): void
    {
        $this->listingTitle = $listingTitle;
    }

    public function toNotificationMessage(): array
    {
        return [
            'email' => $this->email,
            'campaign' => $this->campaign,
            'parameters' => [
                'LANG' => $this->lang,
                'LISTING_TITLE' => $this->listingTitle,
                'LISTING_ID' => $this->listingId,
            ]
        ];
    }
}
