<?php

namespace StepStone\Recruiting\ATS\Notification\Notification;

use StepStone\Queue\QueueMessage;
use StepStone\Recruiting\ATS\Event\Event;
use StepStone\Recruiting\ATS\Event\EventDetails;
use StepStone\Recruiting\ATS\Event\EventType;

class NotificationMessageProcessor
{

    /**
     * @var NotificationService
     */
    private $notificationService;


    /**
     * NotificationMessageProcessor constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function processMessage(QueueMessage $queueMessage): void
    {

        $event = $this->generateEvent($queueMessage);

        $this->notificationService->processEvent($event);
    }

    private function generateEvent(QueueMessage $queueMessage) : Event
    {
        $eventType = new EventType($queueMessage->getMessage()->eventType);
        $eventDetails = new EventDetails($queueMessage->getMessage()->eventDetails);

        return new Event($eventType, $eventDetails);
    }

}
