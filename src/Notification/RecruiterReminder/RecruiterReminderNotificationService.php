<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\RecruiterReminder;

use StepStone\Recruiting\ATS\Event\Event;
use StepStone\Recruiting\ATS\Notification\NotificationModule;
use StepStone\Recruiting\ATS\Notification\NotificationServiceInterface;
use StepStone\Recruiting\ATS\Notification\YamlNotificationServiceConfiguration;
use StepStone\Recruiting\ATS\Recruiter\RecruiterServiceRepository;
use StepStone\Recruiting\ATS\Recruiter\Reminder\HiringReminderCollection;

class RecruiterReminderNotificationService implements NotificationModule
{
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;

    /**
     * @var YamlNotificationServiceConfiguration
     */
    private $config;

    /**
     * @var RecruiterServiceRepository
     */
    private $recruiterServiceRepository;

    /**
     * RecruiterReminderNotificationService constructor.
     *
     * @param NotificationServiceInterface $notificationService
     * @param YamlNotificationServiceConfiguration $config
     * @param RecruiterServiceRepository $recruiterServiceRepository
     */
    public function __construct(
        NotificationServiceInterface $notificationService,
        YamlNotificationServiceConfiguration $config,
        RecruiterServiceRepository $recruiterServiceRepository
    ) {
        $this->notificationService = $notificationService;
        $this->config = $config;
        $this->recruiterServiceRepository = $recruiterServiceRepository;
    }

    public function sendRecruiterContactedReminder(HiringReminderCollection $reminders) : void
    {
        $remindersArray = $reminders->getReminders();
        foreach ($remindersArray as $reminder) {
            $recruitersCollection = $this->recruiterServiceRepository->getListingRecruiters($reminder->getListingId());

            foreach ($recruitersCollection->recruiters as $recruiter) {
                $message = $this->getQueueMessage(
                    $recruiter['email'],
                    $reminder->getListingTitle(),
                    $reminder->getListingId(),
                    $reminder->getApplicationsForSim()
                );
                $messageId = $this->notificationService->getMessageId();
                $this->notificationService->send($messageId, $message);
            }
        }
    }

    private function getQueueMessage(
        string $email,
        string $listingTitle,
        string $listingId,
        array $applications
    ): RecruiterReminderNotificationMessage {
        $notificationMessage = new RecruiterReminderNotificationMessage();
        $notificationMessage->setEmail($email);
        $notificationMessage->setCampaign($this->config->getRecruiterReminderCampaignName());
        $notificationMessage->setLang($this->config->getEmailLanguage());
        $notificationMessage->setListingTitle($listingTitle);
        $notificationMessage->setListingId($listingId);
        $notificationMessage->setAtsUrl($this->getAtsUrl($listingId));
        $notificationMessage->setEmailContent($applications);

        return $notificationMessage;
    }

    private function getAtsUrl(string $listingId) : string
    {
        return 'https://'
            . $this->config->getDomain()
            . '/'
            . $this->config->getAtsBaseUrl($listingId);
    }

    public function sendNotification(Event $event): void
    {
        // TODO: Implement sendNotification() method.
    }
}
