<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Notification\RecruiterReminder;

use StepStone\Recruiting\ATS\Notification\NotificationMessage;

class RecruiterReminderNotificationMessage extends NotificationMessage
{
    /**
     * @var string
     */
    private $atsUrl;

    /**
     * @var array
     */
    private $emailContent;

    /**
     * @return string
     */
    public function getAtsUrl(): string
    {
        return $this->atsUrl;
    }

    /**
     * @param string $atsUrl
     */
    public function setAtsUrl(string $atsUrl)
    {
        $this->atsUrl = $atsUrl;
    }

    /**
     * @return array
     */
    public function getEmailContent(): array
    {
        return $this->emailContent;
    }

    /**
     * @param array $emailContent
     */
    public function setEmailContent(array $emailContent)
    {
        $this->emailContent = $emailContent;
    }

    /**
     * @return array
     */
    public function toNotificationMessage() : array
    {
        $result = parent::toNotificationMessage();
        $result['parameters']['RECRUITER_ATS_LINK'] = $this->atsUrl;
        $result['parameters']['EMAIL_DATA_CONTENT'] = $this->emailContent;
        return $result;
    }
}
