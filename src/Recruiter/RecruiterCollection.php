<?php

namespace StepStone\Recruiting\ATS\Recruiter;

class RecruiterCollection
{
    /**
     * @var Recruiter[]
     */
    public $recruiters;

    /**
     * RecruiterCollection constructor.
     * @param array $recruiters
     */
    public function __construct(array $recruiters = [])
    {
        $this->recruiters = $recruiters;
    }
}
