<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter;

interface RecruiterService
{
    public function getRecruiterDetails(string $recruiterBorsId) : RecruiterDetails;
}
