<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter;

use StepStone\Common\RequestContextInterface;
use StepStone\Common\Web\RestClientFactoryInterface;
use StepStone\Common\Web\RestClientInterface;

class RecruiterServiceRepository implements RecruiterService
{
    private const REST_CLIENT_NAME = 'recruiterServiceRepository';
    private const SERVICE_REQUEST_RECRUITER_PATH = '/bors/user-mgm/api/v1/users/';

    private const APPLY_METHOD_REST_CLIENT_NAME = 'jobApplyMethodRepository';
    private const APPLY_METHOD_SERVICE_REQUEST_RECRUITERS_DETAILS_PATH
        = '/v1/job-applications/method/for-listing/{listingId}/contact-persons';

    /**
     * @var RestClientInterface
     */
    private $restClient;
    private $applyMethodClient;

    /**
     * @var RequestContextInterface
     */
    private $requestContext;

    /**
     * RecruiterServiceRepository constructor.
     *
     * @param RestClientFactoryInterface $clientFactory
     * @param RequestContextInterface $requestContext
     *
     * @throws \StepStone\Common\StepStoneRuntimeException
     */
    public function __construct(
        RestClientFactoryInterface $clientFactory,
        RequestContextInterface $requestContext
    ) {
        $this->restClient = $clientFactory->getClient(static::REST_CLIENT_NAME);
        $this->applyMethodClient = $clientFactory->getClient(static::APPLY_METHOD_REST_CLIENT_NAME);
        $this->requestContext = $requestContext;
    }

    /**
     * @param $listingId
     * @return RecruiterCollection
     */
    public function getListingRecruiters($listingId): RecruiterCollection
    {
        $requestUrl = str_replace(
            '{listingId}',
            $listingId,
            static::APPLY_METHOD_SERVICE_REQUEST_RECRUITERS_DETAILS_PATH
        );

        // TODO should I check if response is correct or no?
        $rawResponse = $this->applyMethodClient->getJsonClass($requestUrl);

        $recruiters = [];
        foreach ($rawResponse->contactPersons as $recruiter) {
            $recruiters[] = [
                'email' => $recruiter->email
            ];
        }

        return new RecruiterCollection($recruiters);
    }


    public function getRecruiterDetails(string $recruiterBorsId): RecruiterDetails
    {
        $requestUrl = static::SERVICE_REQUEST_RECRUITER_PATH . $recruiterBorsId;
        $headers = [
            'stepstone-bid' => $this->getUUID(),
        ];

        $response = $this->restClient->getJsonClass(
            $requestUrl,
            $headers
        );

        $recruiterDetails = RecruiterDetails::getFromStdClass($response);

        return $recruiterDetails;
    }

    private function getUUID() : string
    {
        return $this->requestContext->getRequestId()->toString();
    }
}
