<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter;

class RecruiterDetails
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    // TODO maybe status should be an enum
    /**
     * @var string
     */
    private $status;

    // TODO maybe language should be an enum
    /**
     * @var string
     */
    private $preferredLanguage;

    // TODO maybe gender should be an enum
    /**
     * @var string
     */
    private $gender;

    // TODO maybe userType should be an enum
    /**
     * @var string
     */
    private $userType;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string $preferredLanguage
     */
    public function setPreferredLanguage(string $preferredLanguage): void
    {
        $this->preferredLanguage = $preferredLanguage;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @param string $userType
     */
    public function setUserType(string $userType): void
    {
        $this->userType = $userType;
    }

    /**
     * Get class from stdClass
     *
     * @param \stdClass $src
     * @return RecruiterDetails
     */
    public static function getFromStdClass(\stdClass $src): RecruiterDetails
    {
        $result = new RecruiterDetails();
        $result->setId($src->id);
        $result->setLogin($src->login);
        $result->setEmail($src->email);
        $result->setFirstName($src->firstName);
        $result->setLastName($src->lastName);
        $result->setStatus($src->status);
        $result->setPreferredLanguage($src->preferredLanguage);
        $result->setGender($src->gender);
        $result->setUserType($src->userType);

        return $result;
    }
}
