<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter;

/**
 * @codeCoverageIgnore
 */
class Recruiter
{
    /**
     * @var string
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
