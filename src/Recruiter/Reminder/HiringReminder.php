<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

use StepStone\Recruiting\ATS\Application\Apply\ApplyApplication;
use StepStone\Recruiting\ATS\Listing\ListingDetails;

/**
 * @codeCoverageIgnore
 */
class HiringReminder
{

    /**
     * @var string
     */
    private $listingId;

    /**
     * @var string
     */
    private $listingTitle;

    /**
     * @var string
     */
    private $listingCity;

    /**
     * @var ApplyApplication[]
     */
    private $applications;

    /**
     * HiringReminder constructor.
     * @param ListingDetails $listing
     */
    public function __construct(ListingDetails $listing)
    {
        $this->listingId = $listing->getId();
        $this->listingTitle = $listing->getTitle();
        $this->listingCity = $listing->getCity();
    }

    /**
     * @return string
     */
    public function getListingId(): string
    {
        return $this->listingId;
    }

    /**
     * @return string
     */
    public function getListingTitle(): string
    {
        return $this->listingTitle;
    }

    /**
     * @return string
     */
    public function getListingCity(): string
    {
        return $this->listingCity;
    }

    /**
     * @return ApplyApplication[]
     */
    public function getApplications(): array
    {
        return $this->applications;
    }

    /**
     * @return array
     */
    public function getApplicationsForSim(): array
    {
        $applications = [];

        foreach ($this->applications as $application) {
            $simApplication = [];
            $simApplication['CANDIDATE_FIRSTNAME'] = $application->candidateFirstName;
            $simApplication['CANDIDATE_LASTNAME'] = $application->candidateLastName;
            $simApplication['STATUS'] = $application->status->getValue();

            $applications[] = $simApplication;
        }

        return $applications;
    }

    /**
     * @param $application
     */
    public function addApplication($application): void
    {
        $this->applications[] = $application;
    }
}
