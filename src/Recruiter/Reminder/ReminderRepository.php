<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

interface ReminderRepository
{
    public function addApplicationForReminder(ReminderApplications $applications) : bool;
    public function removeApplicationFromReminder(int $applicationId) : bool;
    public function getApplicationsWithOverdueStatus() : ReminderApplications;
    public function getReminders() : HiringReminderCollection;
    public function setRemindersDate(ReminderApplications $reminders) : bool;
}
