<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Data\DatabaseConnectionInterface;
use StepStone\Recruiting\ATS\Application\Apply\ApplyRepository;
use StepStone\Recruiting\ATS\Application\Status\ApplicationStatus;
use StepStone\Recruiting\ATS\Listing\ListingRepository;

class DatabaseReminderRepository implements ReminderRepository
{

    private const REMINDER_DAYS_TO_SEND_FIRST_REMINDER = 14;
    private const REMINDER_DAYS_TO_SEND_REST_REMINDERS = 10;

    /**
     * @var DatabaseConnectionInterface
     */
    private $connection;

    /**
     * @var ApplyRepository
     */
    private $applyRepository;

    /**
     * @var ListingRepository
     */
    private $listingRepository;

    /**
     * DatabaseReminderRepository constructor.
     * @param DatabaseConnectionFactoryInterface $connectionFactory
     * @param ApplyRepository $applyRepository
     * @param ListingRepository $listingRepository
     */
    public function __construct(
        DatabaseConnectionFactoryInterface $connectionFactory,
        ApplyRepository $applyRepository,
        ListingRepository $listingRepository
    ) {
        $this->connection = $connectionFactory->getConnection('stepstoneAts');
        $this->applyRepository = $applyRepository;
        $this->listingRepository = $listingRepository;
    }

    /**
     * Removes application from reminder table
     * @param int $applicationId
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function removeApplicationFromReminder(int $applicationId): bool
    {
        $sql = 'DELETE FROM %schema%.Reminder WHERE ApplicationId = :applicationId';

        $databaseResult = $this->connection->prepare($sql);
        $databaseResult->bindValue(':applicationId', $applicationId, \PDO::PARAM_INT);

        return $databaseResult->execute();
    }

    public function addApplicationForReminder(ReminderApplications $applications): bool
    {
        $sql = '
            INSERT INTO
                %schema%.Reminder (ApplicationID, LastReminderDate)
            VALUES
                (
                    :applicationId
                    ,null
                )
        ';

        foreach ($applications->getApplications() as $applicationId) {
            $this->removeApplicationFromReminder($applicationId);

            $insertValues = array(
                ':applicationId' => $applicationId
            );

            $databaseResult = $this->connection->prepare($sql);

            $databaseResult->execute($insertValues);
        }

        return true;
    }

    public function getApplicationsWithOverdueStatus() : ReminderApplications
    {
        $statusNew = new ApplicationStatus(ApplicationStatus::NEW);
        $statusKept = new ApplicationStatus(ApplicationStatus::KEPT);

        $sql = '
            SELECT
              app.[ApplicationID] as [ApplicationID]
            FROM
              %schema%.Application app
              LEFT JOIN %schema%.Reminder rmd ON app.ApplicationID = rmd.ApplicationID
              LEFT JOIN %schema%.Hiring h ON app.ListingId = h.ListingId
            WHERE
                (
                  [Status] = :statusNew
                  OR [Status] = :statusKept
                )
                AND h.HiringOpen = 1
                AND Contacted = 0
                AND app.[ApplyDate] < GETDATE()-cast(:daysForFirstReminder as int)
                AND (
                  rmd.LastReminderDate < GETDATE()-cast(:daysForRestReminders as int)
                  OR isnull(CONVERT(VARCHAR(8),rmd.LastReminderDate,112),0) = 0
                )
        ';

        $databaseResult = $this->connection->prepare($sql);
        $sqlParams = [
            ':statusNew' => $statusNew->getValue(),
            ':statusKept' => $statusKept->getValue(),
            ':daysForFirstReminder' => static::REMINDER_DAYS_TO_SEND_FIRST_REMINDER,
            ':daysForRestReminders' => static::REMINDER_DAYS_TO_SEND_REST_REMINDERS,
        ];

        $databaseResult->execute($sqlParams);

        $applicationsId = array();

        while ($application = $databaseResult->fetch()) {
            $applicationsId[] = $application['ApplicationID'];
        }

        return new ReminderApplications($applicationsId);
    }

    public function getReminders(): HiringReminderCollection
    {
        $sql = '
            SELECT
              app.ListingId as listingId
              ,app.ApplyServiceID as applyId
              ,app.Status as status
            FROM
              %schema%.Reminder rmd
              LEFT JOIN %schema%.Application app ON rmd.ApplicationID = app.ApplicationID
              LEFT JOIN %schema%.Hiring h ON app.ListingId = h.ListingId
              WHERE
                h.HiringOpen = 1 AND
                (rmd.LastReminderDate IS NULL
                OR rmd.LastReminderDate < GETDATE()-cast(:daysForRestReminders as int))
        ';

        $databaseResult = $this->connection->prepare($sql);

        $sqlParams = [
            ':daysForRestReminders' => static::REMINDER_DAYS_TO_SEND_REST_REMINDERS,
        ];

        $databaseResult->execute($sqlParams);

        $listingsId = [];

        $applicants = [];

        while ($reminder = $databaseResult->fetch()) {
            $listingId = $reminder['listingId'];

            $listingsId[] = $listingId;

            $applyApplication = $this->applyRepository->getApplication($reminder['applyId']);
            $applyApplication->status = new ApplicationStatus($reminder['status']);

            $applicants[$listingId][] = $applyApplication;
        }

        $reminders = $this->getRemindersCollection($listingsId);

        $reminders = $this->addApplications($reminders, $applicants);

        return $reminders;
    }


    private function getRemindersCollection(array $listingsId) : HiringReminderCollection
    {
        $reminders = [];

        foreach ($listingsId as $listingId) {
            $reminders[$listingId] = $this->getListingReminder($listingId);
        }

        return new HiringReminderCollection($reminders);
    }

    private function getListingReminder(string $listingId): HiringReminder
    {
        $listingDetails = $this->listingRepository->getListingDetails($listingId);

        return new HiringReminder($listingDetails);
    }

    private function addApplications(HiringReminderCollection $reminders, array $listingApplications)
    {
        foreach ($listingApplications as $listingId => $applications) {
            foreach ($applications as $application) {
                $reminders->getReminders()[$listingId]->addApplication($application);
            }
        }
        return $reminders;
    }

    /**
     * @param ReminderApplications $applications
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function setRemindersDate(ReminderApplications $applications): bool
    {

        $sql = '
            UPDATE %schema%.Reminder
            SET
                [LastReminderDate] = GETDATE()
            WHERE
                [ApplicationID] = :applicationId
        ';

        foreach ($applications->getApplications() as $applicationId) {
            $insertValues = array(
                ':applicationId' => $applicationId
            );

            $databaseResult = $this->connection->prepare($sql);

            $databaseResult->execute($insertValues);
        }

        return true;
    }
}
