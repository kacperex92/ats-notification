<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

/**
 * @codeCoverageIgnore
 */
class ReminderApplications
{
    /**
     * @var array
     */
    private $applications;

    /**
     * ReminderApplications constructor.
     * @param array $applications
     */
    public function __construct(array $applications)
    {
        $this->applications = $applications;
    }

    /**
     * @return array
     */
    public function getApplications(): array
    {
        return $this->applications;
    }
}
