<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

use StepStone\Recruiting\ATS\Notification\RecruiterReminder\RecruiterReminderNotificationService;

class ReminderService
{
    /**
     * @var ReminderRepository
     */
    private $reminderRepository;

    /**
     * @var RecruiterReminderNotificationService
     */
    private $notificationService;

    /**
     * ReminderService constructor.
     * @param ReminderRepository $reminderRepository
     * @param RecruiterReminderNotificationService $notificationService
     */
    public function __construct(
        ReminderRepository $reminderRepository,
        RecruiterReminderNotificationService $notificationService
    ) {
        $this->reminderRepository = $reminderRepository;
        $this->notificationService = $notificationService;
    }

    /**
     * Takes care of sending reminders to recruiters
     * @return bool
     */
    public function sendReminders() : bool
    {
        $overdueApplications = $this->reminderRepository->getApplicationsWithOverdueStatus();

        if (!empty($overdueApplications->getApplications())) {
            $this->reminderRepository->addApplicationForReminder($overdueApplications);

            $reminders = $this->reminderRepository->getReminders();

            $this->notificationService->sendRecruiterContactedReminder($reminders);

            $this->reminderRepository->setRemindersDate($overdueApplications);
        }

        return true;
    }

    /**
     * @param int $applicationId
     * @return bool
     */
    public function removeReminder(int $applicationId) : bool
    {
        return $this->reminderRepository->removeApplicationFromReminder($applicationId);
    }
}
