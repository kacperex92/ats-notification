<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

/**
 * @codeCoverageIgnore
 */
class HiringReminderCollection
{

    /**
     * @var HiringReminder[]
     */
    private $reminders;

    /**
     * HiringReminderCollection constructor.
     * @param HiringReminder[] $reminders
     */
    public function __construct(array $reminders)
    {
        $this->reminders = $reminders;
    }

    /**
     * @return HiringReminder[]
     */
    public function getReminders(): array
    {
        return $this->reminders;
    }
}
