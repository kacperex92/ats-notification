<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Recruiter\Reminder;

use StepStone\Recruiting\ATS\Application\Status\ApplicationStatus;

/**
 * @codeCoverageIgnore
 */
class ReminderData
{
    /**
     * @var int
     */
    private $listingId;

    /**
     * @var array
     */
    private $applyId;

    /**
     * @var int
     */
    private $status;

    /**
     * Reminder constructor.
     * @param mixed $reminder
     * @throws \Exception
     */
    public function __construct(\stdClass $reminder)
    {
        $this->listingId = $reminder->listingId;
        $this->applyId = $reminder->applyId;
        $this->status = new ApplicationStatus($reminder->status);
    }
}
