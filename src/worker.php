<?php

require_once __DIR__ . '/../vendor/autoload.php';

use StepStone\Recruiting\ATS\Notification\Worker\DirectQueueWorker;
use StepStone\Recruiting\ATS\Notification\Worker\QueueWorker;

$worker = new QueueWorker(__DIR__);
$worker->run(DirectQueueWorker::class, 'notifications');
