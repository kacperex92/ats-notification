<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Listing;

use StepStone\Common\StepStoneRuntimeException;

/**
 * @codeCoverageIgnore
 */
class ListingMissingException extends StepStoneRuntimeException
{
    // intentionally empty
}
