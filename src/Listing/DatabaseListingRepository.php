<?php

namespace StepStone\Recruiting\ATS\Listing;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Data\DatabaseConnectionInterface;

class DatabaseListingRepository implements ListingRepository
{
    private const LEGACY_CONNECTION_NAME = 'stepstoneListing';

    private const SERVICE_CONNECTION_NAME = 'stepstoneAts';

    /**
     * @var DatabaseConnectionInterface
     */
    private $legacyConnection;

    /**
     * @var DatabaseConnectionInterface
     */
    private $serviceConnection;

    public function __construct(
        DatabaseConnectionFactoryInterface $connectionFactory
    ) {
        $this->legacyConnection = $connectionFactory->getConnection(static::LEGACY_CONNECTION_NAME);
        $this->serviceConnection = $connectionFactory->getConnection(static::SERVICE_CONNECTION_NAME);
    }

    public function getListingDetails(string $listingId): ListingDetails
    {
        $basicListingInfo = $this->getRawDatabaseDataForListing($listingId);

        $hiringStatus = $this->getHiringStatusForListing($listingId);

        return new ListingDetails(
            $basicListingInfo['listingId'],
            $basicListingInfo['listingName'],
            $basicListingInfo['city'],
            $hiringStatus,
            []
        );
    }

    /**
     * @param string $listingId
     *
     * @return array
     * @throws \StepStone\Recruiting\ATS\Listing\ListingMissingException
     * @throws \InvalidArgumentException
     */
    private function getRawDatabaseDataForListing(string $listingId) : array
    {

        $listingBasicInformationRequest = $this->legacyConnection->prepare(
            "SELECT TOP 1
                            o.ID as listingId,
                            ISNULL(o.jobLocation, '') as city,
                            ISNULL(o.name, '') as listingName
                        FROM
                          %schema%.Offer o
                        WHERE
                            o.id = :listingId"
        );

        $listingBasicInformationRequest->bindValue(
            ':listingId',
            $listingId,
            \PDO::PARAM_INT
        );

        $listingBasicInformationRequest->execute();

        $row = $listingBasicInformationRequest->fetch();

        if ($row) {
            return $row;
        }

        throw new ListingMissingException("Cannot find listing with id = $listingId");
    }

    /**
     * @param string $listingId
     *
     * @return \StepStone\Recruiting\ATS\Listing\ListingHiringStatus
     * @throws \InvalidArgumentException
     */
    public function getHiringStatusForListing(string $listingId) : ListingHiringStatus
    {
        $this->insertHiringStatusForListing($listingId);

        $hiringStatusRequest = $this->serviceConnection->prepare(
            'SELECT HiringOpen
                       FROM %schema%.Hiring
                       WHERE ListingId = :listingId'
        );

        $hiringStatusRequest->bindValue(
            ':listingId',
            (int)$listingId,
            \PDO::PARAM_INT
        );

        $hiringStatusRequest->execute();

        $row = $hiringStatusRequest->fetch();

        return new ListingHiringStatus($row['HiringOpen']);
    }

    public function insertHiringStatusForListing(string $listingId) : bool
    {
        $hiringStatus = new ListingHiringStatus(ListingHiringStatus::OPENED);

        $hiringStatusRequest = $this->serviceConnection->prepare(
            'IF NOT EXISTS(SELECT * FROM %schema%.Hiring WHERE ListingID = :listingId)
            INSERT INTO %schema%.Hiring (ListingId, HiringOpen)
            VALUES (:listingId, :hiringStatus)'
        );

        $hiringStatusRequest->bindValue(
            ':listingId',
            (int)$listingId,
            \PDO::PARAM_INT
        );

        $hiringStatusRequest->bindValue(
            ':hiringStatus',
            $hiringStatus->getValue(),
            \PDO::PARAM_INT
        );

        return $hiringStatusRequest->execute();
    }

    /**
     * @param string $listingId
     * @param \StepStone\Recruiting\ATS\Listing\ListingHiringStatus $hiringStatus
     *
     * @return bool
     * @throws \StepStone\Recruiting\ATS\Listing\ListingMissingException
     * @throws \InvalidArgumentException
     */
    public function updateHiringStatusForListing(string $listingId, ListingHiringStatus $hiringStatus) : bool
    {
        $this->getRawDatabaseDataForListing($listingId);

        $hiringStatusRequest = $this->serviceConnection->prepare(
            'UPDATE %schema%.Hiring 
                       SET HiringOpen = :hiringStatus 
                       WHERE ListingId = :listingId'
        );

        $hiringStatusRequest->bindValue(
            ':listingId',
            (int)$listingId,
            \PDO::PARAM_INT
        );

        $hiringStatusRequest->bindValue(
            ':hiringStatus',
            $hiringStatus->getValue(),
            \PDO::PARAM_INT
        );

        return $hiringStatusRequest->execute();
    }
}
