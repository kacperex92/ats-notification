<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Listing;

use Esky\Enum\Enum;

/**
 * @codeCoverageIgnore
 */
class ListingHiringStatus extends Enum
{
    const CLOSED = 0;
    const OPENED = 1;
}
