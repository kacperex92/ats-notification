<?php
/**
 * Copyright Stepstone GmbH
 */

namespace StepStone\Recruiting\ATS\Listing;

use StepStone\Common\StepStoneRuntimeException;

/**
 * Class HiringClosedException
 * @package StepStone\Recruiting\ATS\Listing
 * @codeCoverageIgnore
 */
class HiringClosedException extends StepStoneRuntimeException
{

}
