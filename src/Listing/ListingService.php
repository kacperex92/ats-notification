<?php

namespace StepStone\Recruiting\ATS\Listing;

use StepStone\Recruiting\ATS\Application\ApplicationRepository;
use StepStone\Recruiting\ATS\Hiring\HiringService;
use StepStone\Recruiting\ATS\Notification\NotificationData;

class ListingService
{
    /**
     * @var ApplicationRepository
     */
    private $listingRepository;

    /**
     * @var HiringService
     */
    private $hiringService;

    public function __construct(
        ListingRepository $listingRepository,
        HiringService $hiringService
    ) {
        $this->listingRepository = $listingRepository;
        $this->hiringService = $hiringService;
    }

    /**
     * @param string $listingId
     * @return ListingDetails
     */
    public function getListingDetails(string $listingId): ListingDetails
    {
        return $this->listingRepository->getListingDetails($listingId);
    }

    /**
     * @param string $listingId
     * @throws HiringClosedException
     */
    public function validateOpenHiring(string $listingId) : void
    {
        $listing = $this->getListingDetails($listingId);
        if (!$listing->isHiringOpen()) {
            throw new HiringClosedException('This action can\'t be executed when hiring is closed');
        }
    }

    /**
     * @param string $listingId
     * @param ListingHiringStatus $hiringStatus
     *
     * @param string $recruiterId
     * @param \StepStone\Recruiting\ATS\Notification\NotificationData $notificationData
     *
     * @return bool
     * @throws \Exception
     * @throws \StepStone\Recruiting\ATS\Listing\ListingMissingException
     */
    public function setHiringStatus(
        string $listingId,
        ListingHiringStatus $hiringStatus,
        string $recruiterId,
        NotificationData $notificationData
    ): bool {
        //@TODO: Open hiring should be able without notificationData
        $this->listingRepository->updateHiringStatusForListing($listingId, $hiringStatus);

        if ($hiringStatus->isEqual(new ListingHiringStatus(ListingHiringStatus::CLOSED))) {
            $this->hiringService->sendNotificationToCandidates($listingId, $recruiterId, $notificationData);
        }

        return true;
    }
}
