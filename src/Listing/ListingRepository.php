<?php

namespace StepStone\Recruiting\ATS\Listing;

interface ListingRepository
{
    public function getListingDetails(string $jobApplicationId) : ListingDetails;
    public function getHiringStatusForListing(string $listingId) : ListingHiringStatus;
    public function insertHiringStatusForListing(string $listingId) : bool;

    /**
     * @param string $listingId
     * @param \StepStone\Recruiting\ATS\Listing\ListingHiringStatus $hiringStatus
     *
     * @return bool
     * @throws \StepStone\Recruiting\ATS\Listing\ListingMissingException
     */
    public function updateHiringStatusForListing(string $listingId, ListingHiringStatus $hiringStatus) : bool;
}
