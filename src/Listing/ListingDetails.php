<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Listing;

/**
 * Class ListingDetails
 * @codeCoverageIgnore
 */
class ListingDetails
{
    /**
     * listing id key
     *
     * @var string
     * Example: 12
     * @required
     */
    private $id;

    /**
     * listing title (Offer name)
     *
     * @var string
     * Example: Some offer
     * @required
     */
    private $title;

    /**
     * city where offer is available
     *
     * @var string
     * Example: Berlin
     * @required
     */
    private $city;

    /**
     * @var ListingHiringStatus
     */
    private $hiringStatus;


    /**
     * email(s) of recruiter(s)
     *
     * @var string[]
     * Example: ['email@email.com']
     */
    private $recruiterEmails;

    /**
     * ListingDetails constructor.
     * @param string $id
     * @param string $title
     * @param string $city
     * @param ListingHiringStatus $hiringStatus
     * @param string[] $recruiterEmails
     */
    public function __construct(
        string $id,
        string $title,
        string $city,
        ListingHiringStatus $hiringStatus,
        array $recruiterEmails
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->city = $city;
        $this->hiringStatus = $hiringStatus;
        $this->recruiterEmails = $recruiterEmails;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return bool
     */
    public function isHiringOpen(): bool
    {
        return $this->hiringStatus->isEqual(ListingHiringStatus::OPENED);
    }

    /**
     * @return string[]
     */
    public function getRecruiterEmails() : array
    {
        return $this->recruiterEmails;
    }

    /**
     * @param string[] $recruiterEmails
     */
    public function setRecruiterEmails(array $recruiterEmails): void
    {
        $this->recruiterEmails = $recruiterEmails;
    }

    /**
     * @return ListingHiringStatus
     */
    public function getHiringStatus(): ListingHiringStatus
    {
        return $this->hiringStatus;
    }
}
