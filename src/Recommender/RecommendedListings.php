<?php
/**
 * Copyright Stepstone GmbH
 */

namespace StepStone\Recruiting\ATS\Recommender;

/**
 * Class RecommendedListings
 * @package StepStone\Recruiting\ATS\Recommender
 * @codeCoverageIgnore
 */
class RecommendedListings
{
    /**
     * @var array
     */
    private $listings;

    /**
     * RecommendedListings constructor.
     * @param array $listings
     */
    public function __construct(array $listings)
    {
        $this->listings = $listings;
    }

    /**
     * @return array
     */
    public function getListings() : array
    {
        return $this->listings;
    }
}
