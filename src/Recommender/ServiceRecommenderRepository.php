<?php

namespace StepStone\Recruiting\ATS\Recommender;

use Assert\Assert;
use StepStone\Common\FileSystem\FileSystemFactoryInterface;
use StepStone\Common\Web\RestClientFactoryInterface;
use StepStone\Common\Web\RestClientInterface;
use StepStone\Recruiting\ATS\Notification\YamlNotificationServiceConfiguration;

class ServiceRecommenderRepository
{
    private const REST_CLIENT_NAME = 'recommenderServiceRepository';
    private const REST_PATH_TO_LISTING_DATA = '/recommender/core/{country}/{environment}/listing/';

    private const COUNTRY_CONFIG_NAME = 'country';
    private const ENVIRONMENT_CONFIG_NAME = 'environment';

    private const FILE_SYSTEM_NAME = 'businessConfiguration';
    private const RECOMMENDER_CONFIG_FILE_NAME = 'recommender.yaml';

    /**
     * @var RestClientInterface
     */
    private $restClient;

    /**
     * @var YamlNotificationServiceConfiguration
     */
    private $config;

    /**
     * @var string
     */
    private $country;
    private $environment;


    /**
     * ServiceRecommenderRepository constructor.
     * @param RestClientFactoryInterface $clientFactory
     * @param YamlNotificationServiceConfiguration $config
     * @param FileSystemFactoryInterface $connectionFactory
     * @throws \StepStone\Common\StepStoneRuntimeException
     */
    public function __construct(
        RestClientFactoryInterface $clientFactory,
        YamlNotificationServiceConfiguration $config,
        FileSystemFactoryInterface $connectionFactory
    ) {
        $this->restClient = $clientFactory->getClient(static::REST_CLIENT_NAME);
        $this->config = $config;

        $cacheConnection = $connectionFactory->getFileCacheService(static::FILE_SYSTEM_NAME);
        $this->parseConfig($cacheConnection->parseCacheableYaml(static::RECOMMENDER_CONFIG_FILE_NAME));
    }

    /**
     * @param string $listingId
     * @return RecommendedListings
     */
    public function getRecommendedListings(string $listingId) : RecommendedListings
    {
        $result = [];
        $response = $this->restClient->getJsonClass(
            $this->getServiceUrl()
            . $listingId
            . '?top=' . (string)$this->config->getNumberOfRequestedRecommendedListings()
        );

        foreach ($response->recommendations as $recommendation) {
            $result[] = $recommendation;
        }
        return new RecommendedListings($result);
    }

    private function getServiceUrl() : string
    {
        $template  = static::REST_PATH_TO_LISTING_DATA;
        $keywords = array('{country}', '{environment}');
        $values   = array($this->country, $this->environment);

        return str_replace($keywords, $values, $template);
    }

    private function parseConfig(array $rawConfig) : void
    {
        $this->country = $this->getStringValue(
            static::COUNTRY_CONFIG_NAME,
            $rawConfig
        );
        $this->environment = $this->getStringValue(
            static::ENVIRONMENT_CONFIG_NAME,
            $rawConfig
        );
    }

    private function getStringValue(string $keyName, array $rawConfig) : string
    {
        Assert::that($rawConfig)
            ->keyExists(
                $keyName,
                'Can not find ' . $keyName . ' key in ' . static::RECOMMENDER_CONFIG_FILE_NAME
            );

        $value = $rawConfig[$keyName];

        Assert::that($value)
            ->notEmpty()
            ->string();
        return $value;
    }
}
