<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

/**
 * @codeCoverageIgnore
 */
class ApplicationCollection
{
    /**
     * Job applications
     *
     * @var \stdClass[]
     */
    public $applications;

    /**
     * ApplicationCollection constructor.
     * @param Application[] $applications
     */
    public function __construct(array $applications = [])
    {
        $this->applications = $applications;
    }
}
