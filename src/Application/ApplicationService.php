<?php
/**
 * Copyright Stepstone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

use StepStone\Recruiting\ATS\Application\Apply\ApplyApplication;
use StepStone\Recruiting\ATS\Application\Apply\ApplyRepository;
use StepStone\Recruiting\ATS\Application\Sorting\ApplicationSorter;
use StepStone\Recruiting\ATS\Application\Status\ApplicationCollectionStatus;

class ApplicationService
{

    /**
     * @var ApplicationRepository
     */
    private $applicationRepository;

    /**
     * @var ApplicationSorter
     */
    private $applicationSorter;

    /**
     * @var ApplyRepository
     */
    private $applyRepository;

    /**
     * @var ExternalSourcesDataProvider
     */
    private $externalSourcesDataProvider;

    /**
     * @var ApplicationCombineService
     */
    private $applicationCombineService;

    public function __construct(
        ApplicationRepository $applicationRepository,
        ApplicationSorter $applicationSorter,
        ApplyRepository $applyRepository,
        ExternalSourcesDataProvider $externalSourcesDataProvider,
        ApplicationCombineService $applicationCombineService
    ) {
        $this->applicationRepository = $applicationRepository;
        $this->applicationSorter = $applicationSorter;
        $this->applyRepository = $applyRepository;
        $this->externalSourcesDataProvider = $externalSourcesDataProvider;
        $this->applicationCombineService = $applicationCombineService;
    }

    /**
     * @param int $listingId
     * @param ApplicationCollectionStatus[] $applicationCollectionStatuses
     *
     * @return ApplicationCollection
     */
    public function getApplications(int $listingId, array $applicationCollectionStatuses) : ApplicationCollection
    {
        $externalData = $this->externalSourcesDataProvider->getData($listingId);

        $combinedData = $this->applicationCombineService->combine($externalData);

        $sortedApplications = $this->applicationSorter->sort($combinedData);

        $filterService = new ApplicationFilterService($sortedApplications, $applicationCollectionStatuses);
        return $filterService->getCollection();
    }

    /**
     * @param string $applicationId
     * @return ApplyApplication
     */
    public function getApplication(string $applicationId) :  ApplyApplication
    {
        $applyServiceId = $this->applicationRepository->getApplyServiceId($applicationId);

        return $this->applyRepository->getApplication($applyServiceId);
    }

    /**
     * @param int $applicationId
     * @param int $listingId
     * @return bool
     */
    public function applicationBelongsToListing(int $applicationId, int $listingId) : bool
    {
        return $this->applicationRepository->doesApplicationBelongsToListing(
            $applicationId,
            $listingId
        );
    }
}
