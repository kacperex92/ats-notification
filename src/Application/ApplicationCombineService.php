<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

use StepStone\Recruiting\ATS\Conversation\JobMessenger\JobMessengerChatCollection;

class ApplicationCombineService
{
    public function combine(array $externalData) : ApplicationCollection
    {
        // TODO we should refactor this function when we'll tackle technical debt
        $applications = [];

        foreach ($externalData as $type => $module) {
            if ($this->skipModule($type)) {
                continue;
            }
            foreach ($module->applications as $applicationId => $application) {
                $application = (array)$application;
                if (array_key_exists($applicationId, $applications)) {
                    $applications[$applicationId] = array_merge($applications[$applicationId], $application);
                } else {
                    $applications[$applicationId] = $application;
                }
            }
        }

        foreach ($externalData[ApplicationCombineExternalDataKeys::APPLICATIONS]
                     ->applications as $applicationId => $application) {
            unset($application->status);
            unset($application->id);
            $application = (array)$application;
            if (array_key_exists($applicationId, $applications)) {
                $applications[$applicationId] = array_merge($applications[$applicationId], $application);
            }
        }

        if (array_key_exists(ApplicationCombineExternalDataKeys::PROFILES, $externalData)) {
            foreach ($externalData[ApplicationCombineExternalDataKeys::PROFILES]
                         ->applications as $applicationId => $application) {
                $application = (array)$application;
                if (array_key_exists($applicationId, $applications)) {
                    $applications[$applicationId] = array_merge($applications[$applicationId], $application);
                }
            }
        }

        if (array_key_exists(ApplicationCombineExternalDataKeys::JOB_MESSENGER_CHATS, $externalData)) {
            $applications = $this->combineApplicationsWithJobMessenger(
                $applications,
                $externalData[ApplicationCombineExternalDataKeys::JOB_MESSENGER_CHATS]
            );
        }

        $applicationCollection = [];
        foreach ($applications as $application) {
            $applicationObject = (object)$application;
            $applicationObject->contacted = $applicationObject->contacted->getValue();
            $applicationCollection[] = Application::getFromObject($applicationObject);
        }

        return new ApplicationCollection($applicationCollection);
    }

    private function skipModule(string $type) : bool
    {
        $skipTypes = [
            ApplicationCombineExternalDataKeys::APPLICATIONS,
            ApplicationCombineExternalDataKeys::PROFILES,
            ApplicationCombineExternalDataKeys::JOB_MESSENGER_CHATS
        ];

        return in_array($type, $skipTypes, true);
    }

    private function combineApplicationsWithJobMessenger(
        array $applications,
        JobMessengerChatCollection $jobMessengerChats
    ) : array {
        foreach ($applications as $applicationId => $application) {
            $applications[$applicationId]['jobMessengerAvatarLink'] = null;
            $applications[$applicationId]['jobMessengerChatLink'] = null;
            foreach ((array)$jobMessengerChats->chats as $chat) {
                if (!empty($chat->getApplicant())
                    && $application['candidateEmail'] === $chat->getApplicant()->getEmail()
                ) {
                    $applications[$applicationId]['jobMessengerAvatarLink'] = $chat->getApplicant()->getAvatarLink();
                    $applications[$applicationId]['jobMessengerChatLink'] = $chat->getChatLink();
                    break;
                }
            }
        }

        return $applications;
    }
}
