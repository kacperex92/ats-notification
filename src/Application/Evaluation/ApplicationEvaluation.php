<?php

namespace StepStone\Recruiting\ATS\Application\Evaluation;

/**
 * Domain object representing application evaluation
 *
 * Class ApplicationEvaluation
 * @package StepStone\Recruiting\ATS\Application
 */
class ApplicationEvaluation
{
    /**
     * @var int
     */
    public $applicationId;

    /**
     * @var string
     */
    public $note;

    /**
     * @var int
     */
    public $rating;

    /**
     * @var string
     */
    public $dateCreated;
}
