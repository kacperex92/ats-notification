<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Evaluation;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Data\DatabaseConnectionInterface;

class DatabaseEvaluationRepository implements EvaluationRepository
{
    /**
     * @var DatabaseConnectionInterface
     */
    private $connection;

    public function __construct(DatabaseConnectionFactoryInterface $connectionFactory)
    {
        $this->connection = $connectionFactory->getConnection('stepstoneAts');
    }

    public function getApplicationEvaluations(int $applicationId) : array
    {
        $sql = 'SELECT * FROM %schema%.Note WHERE ApplicationId = :applicationId ORDER BY DateCreated DESC';

        $statement = $this->connection->prepare($sql);
        $statement->bindValue(':applicationId', $applicationId, \PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetchAll();
    }

    public function addApplicationEvaluation(int $applicationId, string $note, ?int $rating) : bool
    {
        $sql ='INSERT INTO %schema%.Note([ApplicationID], [NoteText], [Rating]) 
                VALUES (:applicationId, :note, :rating)';
        $databaseResult = $this->connection->prepare($sql);

        $insertValues = [
            ':applicationId' => $applicationId,
            ':note' => $note,
            ':rating' => $rating,
        ];
        return $databaseResult->execute($insertValues);
    }
}
