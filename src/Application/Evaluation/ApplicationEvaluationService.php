<?php

namespace StepStone\Recruiting\ATS\Application\Evaluation;

use StepStone\Common\InvalidArgumentException;
use StepStone\Recruiting\ATS\Application\Sorting\EvaluationSorter;
use StepStone\Recruiting\ATS\Listing\HiringClosedException;
use StepStone\Recruiting\ATS\Listing\ListingService;

class ApplicationEvaluationService
{
    public const MIN_RATING = 1;
    public const MAX_RATING = 5;

    /**
     * @var EvaluationRepository
     */
    private $repository;

    /**
     * @var EvaluationSorter
     */
    private $evaluationSorter;

    /**
     * @var ListingService
     */
    private $listingService;

    /**
     * ApplicationEvaluationService constructor.
     * @param EvaluationRepository $repository
     * @param EvaluationSorter $evaluationSorter
     * @param ListingService $listingService
     */
    public function __construct(
        EvaluationRepository $repository,
        EvaluationSorter $evaluationSorter,
        ListingService $listingService
    ) {
        $this->repository = $repository;
        $this->evaluationSorter = $evaluationSorter;
        $this->listingService = $listingService;
    }

    /**
     * @param string $applicationId
     * @return ApplicationEvaluation[]
     */
    public function getApplicationEvaluations(string $applicationId) : array
    {
        $evaluations = [];

        $result = $this->repository->getApplicationEvaluations($applicationId);
        foreach ($result as $row) {
            $evaluation = new ApplicationEvaluation();
            $evaluation->dateCreated = $row['DateCreated'];
            $evaluation->note = $row['NoteText'];
            $evaluation->rating = $row['Rating'];

            $evaluations[] = $evaluation;
        }

        return $this->evaluationSorter->sort($evaluations);
    }

    /**
     * @param int $applicationId
     * @param string $note
     * @param int|null $rating
     * @param string $listingId
     * @return bool
     * @throws InvalidArgumentException
     * @throws HiringClosedException
     */
    public function addApplicationEvaluation(int $applicationId, string $note, ?int $rating, string $listingId): bool
    {
        $this->listingService->validateOpenHiring($listingId);

        $this->validateRating($rating);

        return $this->repository->addApplicationEvaluation($applicationId, $note, $rating);
    }

    private function validateRating(?int $rating) : void
    {
        if (null === $rating) {
            return;
        }

        if ($this->isRatingNotInAllowedRange($rating)) {
            throw new InvalidArgumentException(
                sprintf('Wrong value for rating! Allowed from %s to %s', static::MIN_RATING, static::MAX_RATING)
            );
        }
    }

    private function isRatingNotInAllowedRange(int $rating): bool
    {
        return $rating < static::MIN_RATING || $rating > static::MAX_RATING;
    }
}
