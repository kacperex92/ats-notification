<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Evaluation;

/**
 * @codeCoverageIgnore
 */
interface EvaluationRepository
{
    public function getApplicationEvaluations(int $applicationId) : array;

    public function addApplicationEvaluation(int $applicationId, string $note, ?int $rating) : bool;
}
