<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

/**
 * @codeCoverageIgnore
 */
class ApplicationAttachment
{
    /**
     * attachment file name
     *
     * @var string
     * @required
     */
    public $name;

    /**
     * attachment type
     *
     * @var string
     */
    public $type;

    /**
     * In response the following header fields will be set:File name will be set:
     *  - 'Content-Disposition':
     *  attachment file name
     *  - 'Content-Type': file's MIME Type
     *  This URL is not meant to be permanent and sh *
     * Could not be cached. It will be valid for approximately 1 day.
     *
     * @var string
     * Example: http://svc.stepstone.dev:31118/api/attachments/f9edacfb-c5f0-4f9e-af55-de5d49c55af3
     * @required
     */
    public $url;

    /**
     * Get class from stdClass
     *
     * @param \stdClass $src
     * @return ApplicationAttachment
     */
    public static function getFromStdClass(\stdClass $src): ApplicationAttachment
    {
        $result = new ApplicationAttachment();
        $result->name = $src->name;
        $result->type = $src->type;
        $result->url = $src->url;

        return $result;
    }
}
