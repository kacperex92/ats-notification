<?php

namespace StepStone\Recruiting\ATS\Application\Apply;

use StepStone\Common\Web\RestClientFactoryInterface;
use StepStone\Common\Web\RestClientInterface;
use StepStone\Recruiting\ATS\Application\ApplicationCollection;

class ServiceApplyRepository implements ApplyRepository
{

    private const REST_CLIENT_NAME = 'jobApplicationRepository';
    private const SERVICE_REQUEST_APPLICATIONS_PATH = '/v1/job-applications/by-listing/';
    private const SERVICE_REQUEST_APPLICATION_PATH = '/v1/job-applications/';

    /**
     * @var RestClientInterface
     */
    private $restClient;

    /**
     * JobApplicationRepository constructor.
     * @param RestClientFactoryInterface $clientFactory
     * @throws \StepStone\Common\StepStoneRuntimeException
     */
    public function __construct(RestClientFactoryInterface $clientFactory)
    {
        $this->restClient = $clientFactory->getClient(static::REST_CLIENT_NAME);
    }

    /**
     * @param string $listingId
     * @return ApplicationCollection
     */
    public function getApplications(string $listingId) :  ApplicationCollection
    {
        $result = new ApplicationCollection();

        $requestUrl = static::SERVICE_REQUEST_APPLICATIONS_PATH . $listingId;

        $rawResponse = $this->restClient->getJsonClass($requestUrl);

        /** @var ApplicationCollection $rawResponse */
        foreach ($rawResponse->applications as $application) {
            $applicationId = (string)$application->id;
            $result->applications[$applicationId] = ApplyApplication::getFromStdClass($application);
        }

        return $result;
    }

    /**
     * @param string $applyServiceId
     * @return ApplyApplication
     */
    public function getApplication(string $applyServiceId) :  ApplyApplication
    {
        $requestUrl = static::SERVICE_REQUEST_APPLICATION_PATH . $applyServiceId;

        $rawResponse = $this->restClient->getJsonClass($requestUrl);

        return ApplyApplication::getFromStdClass($rawResponse);
    }
}
