<?php

namespace StepStone\Recruiting\ATS\Application\Apply;

use StepStone\Recruiting\ATS\Application\ApplicationCollection;

/**
 * @codeCoverageIgnore
 */
interface ApplyRepository
{
    public function getApplications(string $listingId) : ApplicationCollection;
    public function getApplication(string $applyServiceId) : ApplyApplication;
}
