<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Apply;

use StepStone\Recruiting\ATS\Application\ApplicationAttachment;

/**
 * @codeCoverageIgnore
 */
class ApplyApplication
{

    /**
     * Apply application id
     * @var string
     */
    public $id;

    /**
     * Job application message to recruiter
     *
     * @var string
     * Example: Example job application message
     * @required
     */
    public $candidateMessage;

    /**
     * Candidate first name
     *
     * @var string
     * Example: John
     * @required
     */
    public $candidateFirstName;

    /**
     * Candidate last name
     *
     * @var string
     * Example: Bravo
     * @required
     */
    public $candidateLastName;

    /**
     * Candidate e-mail
     *
     * @var string
     * Example: john.bravo@stepstone.com
     * @required
     */
    public $candidateEmail;

    /**
     * ID of candidate that has sent the job application (references userLogin.id from country database)
     *
     * @var string
     * Example: 13
     * @required
     */
    public $candidateId;

    /**
     * Status of application
     *
     * @var string
     * Example: new
     * @required
     */
    public $status;

    /**
     * When job application has been sent. Date is returned in ISO 8601 format
     *
     * @var string
     * Example: 2016-10-04T14:04:36Z
     * @required
     */
    public $whenApplied;

    /**
     * Application attachments
     *
     * @var array
     * @required
     */
    public $attachments;

    /**
     * Hide from recruiter
     *
     * @var bool
     * @required
     */
    public $hideFromRecruiter;

    /**
     * Is the application anonymous or not ?
     *
     * @var bool
     * @required
     */
    public $anonymousApplication;

    /**
     * Language of the application
     *
     * @var string
     * @required
     */
    public $applicationLanguage;

    /**
     * Get class from stdClass
     *
     * @param \stdClass $src
     * @return ApplyApplication
     */
    public static function getFromStdClass(\stdClass $src): ApplyApplication
    {
        $result = new ApplyApplication();
        $result->id = $src->id;
        $result->candidateMessage = $src->candidateMessage;
        $result->candidateFirstName = $src->candidateFirstName;
        $result->candidateLastName = $src->candidateLastName;
        $result->candidateEmail = $src->candidateEmail;
        $result->candidateId = $src->candidateId;
        $result->whenApplied = $src->whenApplied;
        foreach ($src->attachments as $attachment) {
            $result->attachments[] = ApplicationAttachment::getFromStdClass($attachment);
        }
        $result->hideFromRecruiter = $src->hideFromRecruiter;
        $result->anonymousApplication = $src->anonymousApplication;
        $result->applicationLanguage = $src->applicationLanguage;


        return $result;
    }
}
