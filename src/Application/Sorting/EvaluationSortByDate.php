<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Sorting;

use StepStone\Recruiting\ATS\Application\Evaluation\ApplicationEvaluation;

class EvaluationSortByDate implements EvaluationSorter
{
    private const KEY_USED_TO_SORT = 'dateCreated';
    private const DESCENDING_ORDER = true;

    /**
     * @param array ApplicationEvaluation[]
     * @return ApplicationEvaluation[]
     */
    public function sort(array $applicationEvaluationCollection): array
    {
        usort(
            $applicationEvaluationCollection,
            SortingService::sortByDate(static::KEY_USED_TO_SORT, static::DESCENDING_ORDER)
        );

        return $applicationEvaluationCollection;
    }
}
