<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Sorting;

use StepStone\Recruiting\ATS\Application\ApplicationCollection;

class ApplicationSortByDate implements ApplicationSorter
{
    private const KEY_USED_TO_SORT = 'whenApplied';
    private const DESCENDING_ORDER = true;

    public function sort(ApplicationCollection $applicationCollection): ApplicationCollection
    {
        usort(
            $applicationCollection->applications,
            SortingService::sortByDate(static::KEY_USED_TO_SORT, static::DESCENDING_ORDER)
        );

        return $applicationCollection;
    }
}
