<?php

namespace StepStone\Recruiting\ATS\Application\Sorting;

use StepStone\Recruiting\ATS\Application\ApplicationCollection;

interface ApplicationSorter
{
    public function sort(ApplicationCollection $applicationCollection) : ApplicationCollection;
}
