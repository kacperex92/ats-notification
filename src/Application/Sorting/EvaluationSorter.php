<?php

namespace StepStone\Recruiting\ATS\Application\Sorting;

use StepStone\Recruiting\ATS\Application\Evaluation\ApplicationEvaluation;

interface EvaluationSorter
{
    /**
     * @param array ApplicationEvaluation[]
     * @return ApplicationEvaluation[]
     */
    public function sort(array $applicationEvaluationCollection) : array;
}
