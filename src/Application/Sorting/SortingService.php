<?php

namespace StepStone\Recruiting\ATS\Application\Sorting;

class SortingService
{
    /**
     * @param string $dateKey
     * @param bool $descending
     * @return callable
     */
    public static function sortByDate(string $dateKey, bool $descending): callable
    {
        return function ($firstObject, $secondObject) use ($dateKey, $descending) {

            $date1 = new \DateTime($firstObject->$dateKey);
            $date2 = new \DateTime($secondObject->$dateKey);

            $result = $date1 <=> $date2;

            return $descending
                ? $result * -1
                : $result;
        };
    }
}
