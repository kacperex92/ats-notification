<?php
/**
* Copyright StepStone GmbH
*/

namespace StepStone\Recruiting\ATS\Application;

use StepStone\Recruiting\ATS\Application\Apply\ApplyRepository;
use StepStone\Recruiting\ATS\Application\Contact\ContactRepository;
use StepStone\Recruiting\ATS\Application\Profile\ApplicationProfileService;
use StepStone\Recruiting\ATS\Application\Rating\RatingRepository;
use StepStone\Recruiting\ATS\Application\Status\StatusRepository;
use StepStone\Recruiting\ATS\Conversation\JobMessenger\JobMessengerRepository;
use StepStone\Recruiting\ATS\Features\Configuration\FeaturesConfiguration;
use StepStone\Recruiting\ATS\Listing\ListingRepository;

class ExternalSourcesDataProvider
{
    /**
     * @var ApplyRepository
     */
    private $applyRepository;

    /**
     * @var StatusRepository
     */
    private $statusRepository;

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var RatingRepository
     */
    private $ratingRepository;

    /**
     * @var ApplicationProfileService
     */
    private $applicationProfileService;

    /**
     * @var JobMessengerRepository
     */
    private $jobMessengerRepository;

    /**
     * @var ApplicationRepository
     */
    private $applicationRepository;

    /**
     * @var ListingRepository
     */
    private $listingRepository;

    /**
     * @var FeaturesConfiguration
     */
    private $featuresConfiguration;

    public function __construct(
        ApplyRepository $applyRepository,
        StatusRepository $statusRepository,
        ContactRepository $contactRepository,
        RatingRepository $ratingRepository,
        ApplicationProfileService $applicationProfileService,
        JobMessengerRepository $jobMessengerRepository,
        ApplicationRepository $applicationRepository,
        ListingRepository $listingRepository,
        FeaturesConfiguration $featuresConfiguration
    ) {
        $this->applyRepository = $applyRepository;
        $this->statusRepository = $statusRepository;
        $this->contactRepository = $contactRepository;
        $this->ratingRepository = $ratingRepository;
        $this->applicationProfileService = $applicationProfileService;
        $this->jobMessengerRepository = $jobMessengerRepository;
        $this->applicationRepository = $applicationRepository;
        $this->featuresConfiguration = $featuresConfiguration;
        $this->listingRepository = $listingRepository;
    }

    public function getData(int $listingId) : array
    {
        $externalSourcesData = array();

        $externalSourcesData[ApplicationCombineExternalDataKeys::APPLICATIONS] =
            $this->applyRepository->getApplications($listingId);

        $this->addNewApplicationsToServiceDatabase(
            $externalSourcesData[ApplicationCombineExternalDataKeys::APPLICATIONS],
            $listingId
        );

        $this->listingRepository->insertHiringStatusForListing($listingId);

        $externalSourcesData[ApplicationCombineExternalDataKeys::STATUSES] =
            $this->statusRepository->getStatuses($listingId);

        $externalSourcesData[ApplicationCombineExternalDataKeys::CONTACT_FLAGS] =
            $this->contactRepository->getContactedFlags($listingId);

        $externalSourcesData[ApplicationCombineExternalDataKeys::RATINGS] =
            $this->ratingRepository->getRatingForListing($listingId);

        $externalSourcesData[ApplicationCombineExternalDataKeys::PROFILES] =
            $this->applicationProfileService->getProfilesForApplications(
                $externalSourcesData[ApplicationCombineExternalDataKeys::APPLICATIONS]
            );

        if ($this->featuresConfiguration->isJobmessengerIntegrationEnabled()) {
            $externalSourcesData[ApplicationCombineExternalDataKeys::JOB_MESSENGER_CHATS] =
            $this->jobMessengerRepository->getListingChats($listingId);
        }

        return $externalSourcesData;
    }

    /**
     * Temporary solutions to keep database synchronized with external sources
     */

    private function addNewApplicationsToServiceDatabase(ApplicationCollection $applications, string $listingId) : void
    {
        $this->applicationRepository->addNewApplications($applications, $listingId);
    }
}
