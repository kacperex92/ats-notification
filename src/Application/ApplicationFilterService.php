<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

use StepStone\Recruiting\ATS\Application\Status\ApplicationCollectionStatus;
use StepStone\Recruiting\ATS\Application\Status\ApplicationStatus;

class ApplicationFilterService
{
    /**
     * @var \StepStone\Recruiting\ATS\Application\ApplicationCollection
     */
    private $collection;

    /**
     * @var \StepStone\Recruiting\ATS\Application\Status\ApplicationCollectionStatus[]
     */
    private $requestedVariants;


    /**
     * ApplicationFilterService constructor.
     *
     * @param ApplicationCollection $collection
     * @param ApplicationCollectionStatus[] $requestedVariants
     */
    public function __construct(ApplicationCollection $collection, array $requestedVariants)
    {
        $this->collection = $collection;
        $this->requestedVariants = $requestedVariants;

        $this->filter();
    }

    /**
     * @return ApplicationCollection
     */
    public function getCollection(): ApplicationCollection
    {
        return $this->collection;
    }

    private function filter() : void
    {
        $statusesToReturn = $this->getVariantsStatuses($this->requestedVariants);

        foreach ($this->collection->applications as $applicationKey => $applicationValue) {
            if (in_array($applicationValue->status, $statusesToReturn, false)) {
                $applicationValue->status = $applicationValue->status->getName();
                continue;
            }
            unset($this->collection->applications[$applicationKey]);
        }

        $this->collection->applications = array_values($this->collection->applications);
    }

    /**
     * @param ApplicationCollectionStatus[] $applicationCollectionStatuses
     *
     * @return array
     * @throws \Exception
     *
     */
    private function getVariantsStatuses(array $applicationCollectionStatuses) : array
    {
        $applicationStatusNew = new ApplicationStatus(ApplicationStatus::NEW);
        $applicationStatusKept = new ApplicationStatus(ApplicationStatus::KEPT);
        $applicationStatusDiscarded = new ApplicationStatus(ApplicationStatus::DISCARDED);
        $applicationStatusHired = new ApplicationStatus(ApplicationStatus::HIRED);

        $map = [
            ApplicationCollectionStatus::ALL => [
                $applicationStatusNew,
                $applicationStatusKept,
                $applicationStatusDiscarded,
                $applicationStatusHired
            ],
            ApplicationCollectionStatus::NEW => [
                $applicationStatusNew
            ],
            ApplicationCollectionStatus::KEPT => [
                $applicationStatusKept,
                $applicationStatusHired
            ],
            ApplicationCollectionStatus::DISCARDED => [
                $applicationStatusDiscarded
            ],
            ApplicationCollectionStatus::HIRED => [
                $applicationStatusHired
            ],
        ];

        $result = [];

        foreach ($applicationCollectionStatuses as $applicationCollectionStatus) {
            $result = array_merge($result, $map[$applicationCollectionStatus->getValue()]);
        }

        return $result;
    }
}
