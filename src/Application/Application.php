<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

/**
 * @codeCoverageIgnore
 */
class Application
{
    /**
     * Job application key
     *
     * @var string
     * Example: 12
     * @required
     */
    public $id;

    /**
     * Job application message to recruiter
     *
     * @var string
     * Example: Example job application message
     * @required
     */
    public $candidateMessage;

    /**
     * Candidate first name
     *
     * @var string
     * Example: John
     * @required
     */
    public $candidateFirstName;

    /**
     * Candidate last name
     *
     * @var string
     * Example: Bravo
     * @required
     */
    public $candidateLastName;

    /**
     * Candidate e-mail
     *
     * @var string
     * Example: john.bravo@stepstone.com
     * @required
     */
    public $candidateEmail;

    /**
     * ID of candidate that has sent the job application (references userLogin.id from country database)
     *
     * @var string
     * Example: 13
     * @required
     */
    public $candidateId;

    /**
     * Flag indicator if candidate has been contacted through SAM or not
     *
     * @var boolean
     * @required
     */
    public $contacted;

    /**
     * Flag indicator if there was a discarded notification sent to the candidate
     *
     * @var boolean
     * @required
     */
    public $isDiscardedNotificationSent;

    /**
     * Status of application
     *
     * @var string
     * Example: new
     * @required
     */
    public $status;

    /**
     * When job application has been sent. Date is returned in ISO 8601 format
     *
     * @var string
     * Example: 2016-10-04T14:04:36Z
     * @required
     */
    public $whenApplied;

    /**
     * Application attachments
     *
     * @var array
     * @required
     */
    public $attachments;

    /**
     * @var string|null
     */
    public $jobMessengerChatLink;

    /**
     * Get class from stdClass
     *
     * @param \stdClass $src
     * @return Application
     */
    public static function getFromObject(\stdClass $src): Application
    {
        $result = new Application();
        $result->id = $src->id;
        $result->candidateMessage = $src->candidateMessage;
        $result->candidateFirstName = $src->candidateFirstName;
        $result->candidateLastName = $src->candidateLastName;
        $result->candidateEmail = $src->candidateEmail;
        $result->candidateId = $src->candidateId;
        $result->status = $src->status;
        $result->contacted = $src->contacted;
        $result->isDiscardedNotificationSent = $src->isDiscardedNotificationSent === 1;
        $result->whenApplied = $src->whenApplied;
        $result->attachments = $src->attachments;

        $result->jobMessengerChatLink = $src->jobMessengerChatLink ?? null;
        $result->rating = $src->rating ?? null;
        $result->candidateProfile = $src->profile ?? null;


        return $result;
    }
}
