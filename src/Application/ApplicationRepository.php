<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application;

/**
 * @codeCoverageIgnore
 */
interface ApplicationRepository
{
    public function addNewApplications(ApplicationCollection $applicationCollection, int $listingId): bool;

    public function doesApplicationBelongsToListing(int $applicationId, int $listingId) : bool;
    public function getApplyServiceId(string $applicationId) : ?string;
}
