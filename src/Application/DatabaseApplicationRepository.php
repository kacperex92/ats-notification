<?php

namespace StepStone\Recruiting\ATS\Application;

use StepStone\Common\Data\DatabaseConnectionFactoryInterface;
use StepStone\Common\Data\DatabaseConnectionInterface;
use StepStone\Database\DatabaseTransactionException;
use StepStone\Recruiting\ATS\Application\Status\ApplicationStatus;

class DatabaseApplicationRepository implements ApplicationRepository
{

    private const MSSQL_UNIQUE_INDEX_VIOLATION_ERROR_CODE = 2627;
    private const MSSQL_UNIQUE_CONSTRAINT_KEY_VIOLATION_ERROR_CODE = 2601;

    /**
     * @var DatabaseConnectionInterface
     */
    private $connection;

    public function __construct(
        DatabaseConnectionFactoryInterface $connectionFactory
    ) {
        $this->connection = $connectionFactory->getConnection('stepstoneAts');
    }

    public function addNewApplications(ApplicationCollection $applicationCollection, int $listingId): bool
    {
        $sql ='INSERT INTO %schema%.Application
            ([ListingId]
            ,[ApplyServiceID]
            ,[Status])
            VALUES (
                :ListingId
                ,:ApplyServiceID
                ,:Status
            )
        ';

        $statusForNewApplications = new ApplicationStatus(ApplicationStatus::NEW);

        $result = [];

        foreach ($applicationCollection->applications as $applicationId => $application) {
            if (!$application->hideFromRecruiter) {
                $databaseResult = $this->connection->prepare($sql);

                $insertValues = array(
                    ':ListingId' => $listingId,
                    ':ApplyServiceID' => $applicationId,
                    ':Status' => $statusForNewApplications->getValue()
                );

                try {
                    $result[] = $databaseResult->execute($insertValues);
                } catch (\PDOException $exception) {
                    if ($this->isNotUniqueExceptionThrown($exception)) {
                        throw new DatabaseTransactionException($exception);
                    }
                }
            }
        }

        return !in_array(false, $result, true);
    }

    /**
     * @param int $applicationId
     * @param int $listingId
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function doesApplicationBelongsToListing(int $applicationId, int $listingId) : bool
    {
        $connection =$this->connection->prepare(
            'SELECT TOP 1
                            count(ApplicationId) as [belongs]
                        FROM
                            %schema%.Application
                        WHERE
                            ListingId = :listingId
                            AND
                            ApplicationId = :applicationId'
        );

        $connection->bindValue(
            ':listingId',
            $listingId,
            \PDO::PARAM_INT
        );
        $connection->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        $connection->execute();

        return (bool)$connection->fetch()['belongs'];
    }

    private function isNotUniqueExceptionThrown($exception) : bool
    {
        return
            static::MSSQL_UNIQUE_INDEX_VIOLATION_ERROR_CODE !== $exception->errorInfo[1]
            && static::MSSQL_UNIQUE_CONSTRAINT_KEY_VIOLATION_ERROR_CODE !== $exception->errorInfo[1];
    }

    /**
     * @param string $applicationId
     * @return string
     * @throws ApplicationMissingException | \InvalidArgumentException
     */
    public function getApplyServiceId(string $applicationId) : string
    {
        $connection =$this->connection->prepare(
            'SELECT TOP 1
                    ApplyServiceID
                FROM
                    %schema%.Application
                WHERE
                    ApplicationID = :applicationId'
        );

        $connection->bindValue(
            ':applicationId',
            $applicationId,
            \PDO::PARAM_INT
        );

        $connection->execute();

        $row = $connection->fetch();
        if (!$row) {
            throw new ApplicationMissingException("Cannot find application with id = $applicationId");
        }

        return $row['ApplyServiceID'];
    }
}
