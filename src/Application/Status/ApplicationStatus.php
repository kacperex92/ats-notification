<?php

namespace StepStone\Recruiting\ATS\Application\Status;

use Esky\Enum\Enum;

/**
 * @codeCoverageIgnore
 */
class ApplicationStatus extends Enum
{
    const NEW = 1;
    const KEPT = 2;
    const DISCARDED = 3;
    const HIRED = 4;
}
