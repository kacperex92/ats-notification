<?php
/**
 * Copyright StepStone GmbH
 */

namespace StepStone\Recruiting\ATS\Application\Status;

use Esky\Enum\Enum;

class ApplicationCollectionStatus extends Enum
{
    const NEW = 1;
    const KEPT = 2;
    const DISCARDED = 3;
    const HIRED = 4;
    const ALL = 5;
}
